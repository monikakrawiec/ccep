
*** Settings ***
Resource          keywords_myaccount.robot

*** Test Cases ***

Prerequisites
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

C5521: Validate My profile overview
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Go to MyProfile page
    Verify MyProfile tabs visible
    Click My profile tab
    Validate My profile tab sections visibility
    Click My accounts & employees tab
    Validate My accounts & employees tab sections visibility
    Click My business tab
    Validate My business tab sections visibility

C5522: Display personal information
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Go to Salesforce
    Log in to SalesForce
    Open SalesForce classic
    Search for member
    Enter member details
    Remember user data
    Go to CCEP
    Go to MyProfile page
    Click My profile tab
    Verify Personal Information display
    Verify Communication Information displayed

C10453: Edit user information
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Edit profile
    Verify edit password disabled
    Verify fields editable
    Update first name
    Update last name
    Cancel editing
    Verify Personal Information display
    Edit profile
    Update first name
    Update last name
    Discard profile changes
    Verify Personal Information display
    Edit profile
    Update first name
    Update last name
    Save profile changes
    Get first name from Profile form
    Get last name from Profile form
    Verify Personal Information updated
    Edit profile
    Clear mandatory profile data
    Save profile changes
    Verify mandatory fields validation triggered

C10453: Edit telephone without saving
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Go to MyProfile page
    Remember phone number
    Edit profile
    Update phone number
    Discard changes
    Verify phone not updated

C10453: Edit mobile phone without saving
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Remember mobile phone number
    Edit profile
    Update mobile phone number
    Discard changes
    Verify mobile phone not updated

C10453: Edit user password
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Set global variable    ${original_password}    ${ccep_password}
    Edit password
    Submit password form
    Verify mandatory password fields validation triggered
    Verify invalid old password not accepted
    Verify confirmation not matching new password not accepted
    Insert valid values and discard changes
    Go to MyProfile page
    Edit password
    Insert valid values and submit changes
    Go to MyProfile page
    Edit password
    Old password cannot be used as new
    Reset password to original value
    Edit password
    Verify password validation rules

Validate error messages for multi account creation
    #[tags]    NL
    Choose Accounts&Employees tab
    Show 75 items per page
    @{statuses}    Create List    Activo    Actif    Actief    Active
    Set Suite Variable     @{statuses}
    Search for account with chosen status
    Add account
    Verify active account error message
    Close modal
    @{statuses}    Create List    Pendiente    En cours
    Set Suite Variable     @{statuses}
    Search for account with chosen status
    Add account
    Verify pending account error message
