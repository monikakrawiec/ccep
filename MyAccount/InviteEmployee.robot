*** Settings ***
Resource          keywords_myaccount.robot

*** Test Cases ***

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    #Open Browser    ${SITE_URL}    ${browser}  remote_url=${RemoteUrl}  desired_capabilities=${DC}
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Invite an employee
    [tags]    BELUX    NL    FRANCE    GB
    Go to MyProfile page
    Choose account and employees tab
    Remember employees status
    Set global variable    ${all_employees_before}    ${all_employees}
    Set global variable    ${all_before}    ${all}
    Set global variable    ${active_before}    ${active}
    Set global variable    ${invited_before}    ${invited}
    Invite new employee
    Verify next button disabled
    Verify back to MyProfile breadcrumb present
    Fill invite employee form
    Go to next step of invitation form
    Verify next button disabled
    Verify back to MyProfile breadcrumb present
    #Validate role table
    Choose role
    Remember chosen role
    Go to next step of invitation form
    Verify next button disabled
    Verify back to MyProfile breadcrumb present
    Choose outlet
    Remember chosen outlet
    Go to next step of invitation form
    Verify employee invited landing page

Verify employee invitation visible in employees overview
    [tags]    BELUX    NL    FRANCE    GB
    Click Go to My Employees button
    Choose account and employees tab
    Remember employees status
    Set global variable    ${all_employees_after}    ${all_employees}
    Set global variable    ${all_after}    ${all}
    Set global variable    ${active_after}    ${active}
    Set global variable    ${invited_after}    ${invited}
    Verify employees quantity has changed after invitation
    Unfold pagination option for My Employees
    Choose 75 per page
    Go to last page of My Employees table
    Count employees rows
    Check the row of invited employee
    Verify first name for invited employee present in overview table
    Verify last name for invited employee present in overview table
    Verify invited email present in overview table
    Verify role for invited employee present in overview table
    Open active employees tab
    Go to last page of My Employees table
    Verify invited employee not in active employees
    Open invited employees tab
    Go to last page of My Employees table
    Count employees rows
    Check the row of invited employee
    Verify first name for invited employee present in overview table
    Verify last name for invited employee present in overview table
    Verify invited email present in overview table
    Verify role for invited employee present in overview table

Invited employee cannot be re-invited
    [tags]    BELUX    NL    FRANCE    GB
    Invite new employee
    Fill invite employee form with existing email
    Go to next step of invitation form
    Verify duplicated invitation message

Get registration token from SalesForce
    [tags]    BELUX    NL    FRANCE    GB
    Go to Salesforce
    Log in to SalesForce
    Open SalesForce classic
    Search for invited email
    Go to registration request for invited employee
    Remember registration token
    Close browser

Invited employee activates account
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Open activate account link
    Verify activation button disabled
    Enter activation password
    Set suite variable    ${retype_password}    12345#Suyo
    Enter activation password confirmation
    Verify validation triggered for not matching password
    Verify activation button disabled
    Set suite variable    ${retype_password}    ${ccep_password}
    Enter activation password confirmation
    Verify no validation for not matching password
    Submit activation form
    Verify validation triggered for legal checkboxes
    Check terms of use checkbox
    Check privacy policy checkbox
    Check conditions of sale checkbox
    Submit activation form
    Verify employee activation succeeded
    Go to dashboard from welcome page
    Verify employee logged in
    Verify Dashboard shown after log in
    Close browser

Verify employee active
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in
    Go to MyProfile page
    Choose account and employees tab
    Remember employees status
    Verify employees quantity has changed after activation
    Unfold pagination option for My Employees
    Choose 75 per page
    Go to last page of My Employees table
    Count employees rows
    Check the row of invited employee
    Verify first name for invited employee present in overview table
    Verify last name for invited employee present in overview table
    Verify invited email present in overview table
    Verify role for invited employee present in overview table
    Verify added date for invited employee present in overview table
    Open active employees tab
    Go to last page of My Employees table
    Count employees rows
    Check the row of invited employee
    Verify first name for invited employee present in overview table
    Verify last name for invited employee present in overview table
    Verify invited email present in overview table
    Verify role for invited employee present in overview table
    Verify added date for invited employee present in overview table
    Open invited employees tab
    Go to last page of My Employees table
    Go to last page of My Employees table
    Verify invited employee not in invited employees
    Close browser
