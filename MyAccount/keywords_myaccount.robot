*** Settings ***
Resource         ../keywords.robot

** Keywords **

Go to MyProfile page
    SeleniumLibrary.Go To    ${site_url}/myaccount/profile
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'profile view')]
    Sleep    5s

Remember phone number
    ${phone_number}    SeleniumLibrary.Get Text    xpath=//*[@id="tab-myprofile"]/form[1]/div/div[2]/div/div/div[1]/article/div/div/div/div[5]/div/div/span
    Set global variable    ${phone_number}

Remember mobile phone number
    ${mobile_phone_number}    SeleniumLibrary.Get Text    xpath=//*[@id="tab-myprofile"]/form[1]/div/div[2]/div/div/div[1]/article/div/div/div/div[6]/div/div/input
    Set global variable    ${mobile_phone_number}

Edit MyProfile
    SeleniumLibrary.Click Element    xpath=//form[contains(@class, 'profile-user')]/div/div/div/div/div/span

Get last character from phone number
    @{phone_number_list}    Split String To Characters    ${phone_number}
    Set Global Variable    ${last_digit_phone}    ${phone_number_list}[-1]

Update phone number
    Get last character from phone number
    :FOR    ${i}    IN RANGE    0    9
    \    ${last_digit}   Generate Random String    1    [NUMBERS]
    \    Run keyword if    ${last_digit_phone} != ${last_digit}    Set Global Variable    ${last_digit}
    \    Exit For Loop If    ${last_digit_phone} != ${last_digit}
    ${phone_number_updated}    Replace String    ${phone_number}    ${last_digit_phone}    ${last_digit}
    SeleniumLibrary.Input Text    xpath=//div[contains(@class, 'input-phone__wrapper')]/div[2]/div/input    ${phone_number_updated}

Update mobile phone number
    SeleniumLibrary.Input Text    id=ecp-input-profile-phonenumber-mobile    ${phone_number}

Discard changes
    SeleniumLibrary.Click Element    xpath=//a[contains(@class, 'copytext4 d-flex justify-content-center discard-changes')]

Verify phone not updated
    SeleniumLibrary.Element Text Should Be    xpath=//*[@id="tab-myprofile"]/form[1]/div/div[2]/div/div/div[1]/article/div/div/div/div[5]/div/div/span    ${phone_number}

Verify mobile phone not updated
    SeleniumLibrary.Element Text Should Be    xpath=//*[@id="tab-myprofile"]/form[1]/div/div[2]/div/div/div[1]/article/div/div/div/div[6]/div/div/span    ${mobile_phone_number}

Choose Accounts&Employees tab
    SeleniumLibrary.Click Element    id=tab-myemployees___BV_tab_button__
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'ecp-table add-account')]
    Sleep    3s

Show 75 items per page
    SeleniumLibrary.Click Element    xpath=//div[3]/div/button
    SeleniumLibrary.Click Element    css=.show > li:nth-child(4) > .dropdown-item

Count accounts
    ${accounts_count}    SeleniumLibrary.Get Element Count    xpath=//tbody[contains(@role, 'rowgroup')]/tr
    Set Global Variable    ${accounts_count}

Search for account with chosen status
    Count accounts
    :FOR    ${i}    IN RANGE    1    ${accounts_count}
    \    ${account_status}    SeleniumLibrary.Get Text    xpath=//tbody[contains(@role, 'rowgroup')]/tr[${i}]/td[3]/div/div/span
    \    ${account_number}    SeleniumLibrary.Get Text    xpath=//tbody[contains(@role, 'rowgroup')]/tr[${I}]/td[1]/div/div/span
    \    ${check}   Run Keyword And Return Status    Collections.List Should Contain Value    ${statuses}     ${account_status}
    \    Exit For Loop If    '${check}' == 'True'
    Set Global Variable    ${account_number}

Add account
    SeleniumLibrary.Click Element     css=#tab-myemployees .custom-header span
    SeleniumLibrary.Input Text    id=ecp-input-profile-add-account-number    ${account_number}
    SeleniumLibrary.Click Element    css=.d-flex > .btn > div
    Sleep    3s

Verify active account error message
    Run keyword if    '${usr}' == 'Iberia'    SeleniumLibrary.Element Text Should Be    css=.alert    Esta cuenta ya existe.
    Run keyword if    '${usr}' == 'Beaux'    SeleniumLibrary.Element Text Should Be    css=.alert    Ce numéro de client a déjà été ajouté.

Verify pending account error message
    Run keyword if    '${usr}' == 'Iberia'    SeleniumLibrary.Element Text Should Be    css=.alert    Ya hay una solicitud abierta para este número de cuenta.

Close modal
    SeleniumLibrary.Click Element    css=#MODAL_MULTIPLE_ACCOUNT___BV_modal_header_ .fa

Go to MyInvoices page
    Choose MyAccount from top menu
    SeleniumLibrary.Click Element    xpath=(//a[contains(@href, '/myaccount/invoices')])[2]
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'customContainer')]

Choose account and employees tab
    Selenium Library.Click element    id=tab-myemployees___BV_tab_button__
    Selenium Library.Wait until element is visible    id=tab-myemployees
    Sleep    5s

Invite new employee
    Click on invite new employee link

Click on invite new employee link
    Selenium Library.Click element    xpath=//div[contains(@data-testid, 'ccep-div-card-employee')]/span
    Sleep    5s
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center']    Invite Employee
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center']    Invite Employee
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center']    Inviter un collègue
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center']    Invite Employee
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center']    Medewerker uitnodigen
    #Selenium Library.Wait until location is    ${site_url}/user-invitation
    Selenium Library.Location should be    ${site_url}/user-invitation

Verify next button disabled
    Selenium Library.Element should be visible    xpath=//button[@class='btn mb-2 btn-primary btn-block disabled']/div

Fill invite employee form
    ${random_string}   Generate Random String    9    [LETTERS]
    Selenium Library.Input text    id=ecp-input-firstName    QA INVITED
    Selenium Library.Input text    id=ecp-input-lastName    ${usr} INVITED
    Selenium Library.Input text    id=ecp-input-email    suyo.automation+${random_string}@gmail.com
    Insert email confirmation not matching email
    Verify validation triggered for not matching email
    Selenium Library.Input text    id=ecp-input-confirmEmail    suyo.automation+${random_string}@gmail.com
    Verify validation for not matching emails disappeared
    Set global variable    ${invited_email}    suyo.automation+${random_string}@gmail.com
    ${invited_email}    String.Convert to lowercase    ${invited_email}
    Set global variable    ${invited_email}

Fill invite employee form with existing email
    Selenium Library.Input text    id=ecp-input-firstName    QA INVITED
    Selenium Library.Input text    id=ecp-input-lastName    ${usr} INVITED
    Selenium Library.Input text    id=ecp-input-email    ${invited_email}
    Selenium Library.Input text    id=ecp-input-confirmEmail    ${invited_email}
    Sleep    2s

Insert email confirmation not matching email
    Selenium Library.Input text    id=ecp-input-confirmEmail    ${user}
    Sleep    1s

Verify validation triggered for not matching email
    ${email_field_highlighted}    Selenium Library.Get element attribute    id=ecp-input-confirmEmail    class
    BuiltIn.Should be equal as strings    ${email_field_highlighted}    form-control is-invalid
    Selenium Library.Element should be visible    xpath=//div[@class='validation-feedback text-left']/div
    Verify next button disabled

Verify validation for not matching emails disappeared
    Selenium Library.Element should not be visible    xpath=//div[@class='validation-feedback text-left']/div

Go to next step of invitation form
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-submit')]/div
    Sleep    3s

Choose role
   Selenium Library.Wait until element is visible    xpath=//div[@class='ecp-table table--employees']
   Sleep    3s
   Check available roles
   Choose random role

Check available roles
    ${roles_count}    Selenium Library.Get Element Count    xpath=//tbody[@role='rowgroup']/tr
    Set global variable    ${roles_count}

Choose random role
    ${random_int}    Evaluate    random.randint(1, ${roles_count})    modules=random
    Set global variable    ${random_int}
    #Selenium Library.Click element    css=tr:nth-child(${random_int}) .custom-control-label
    #Selenium Library.Click element    css=tr:nth-child(3) .custom-control-label
    Execute Javascript    (function() {document.querySelector("tr:nth-child(${random_int}) .custom-control-label").click();})();

Remember chosen role
    ${chosen_role_name}    Selenium Library.Get text    css=tr:nth-child(${random_int}) > td:nth-child(2) p
    Set global variable    ${chosen_role_name}

Validate role table
    Ordering checked for Manager
    Prices not checked for Manager
    My Invoices checked for Manager
    My Business Details checked for Manager
    Ordering checked for InvoiceManager
    Prices not checked for InvoiceManager
    My Invoices checked for InvoiceManager
    My Business Details not checked for InvoiceManager
    Ordering checked for BuyerWithPrice
    Prices not checked for BuyerWithPrice
    My Invoices not checked for BuyerWithPrice
    My Business Details not checked for BuyerWithPrice
    Ordering checked for BuyerWithoutPrice
    Prices not checked for BuyerWithoutPrice
    My Invoices not checked for BuyerWithoutPrice
    My Business Details not checked for BuyerWithoutPrice

Ordering checked for Manager
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[1]/td[@data-label='Ordering']/div/div/div/p/i

Prices not checked for Manager
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[1]/td[@data-label='Prices']/div/div/div/p/i

My Invoices checked for Manager
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[1]/td[@data-label='My Invoices']/div/div/div/p/i

My Business Details checked for Manager
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[1]/td[@data-label='My Business Details']/div/div/div/p/i

Ordering checked for InvoiceManager
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[2]/td[@data-label='Ordering']/div/div/div/p/i

Prices not checked for InvoiceManager
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[2]/td[@data-label='Prices']/div/div/div/p/i

My Invoices checked for InvoiceManager
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[2]/td[@data-label='My Invoices']/div/div/div/p/i

My Business Details not checked for InvoiceManager
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[2]/td[@data-label='My Business Details']/div/div/div/p/i

Ordering checked for BuyerWithPrice
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[3]/td[@data-label='Ordering']/div/div/div/p/i

Prices not checked for BuyerWithPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[3]/td[@data-label='Prices']/div/div/div/p/i

My Invoices not checked for BuyerWithPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[3]/td[@data-label='My Invoices']/div/div/div/p/i

My Business Details not checked for BuyerWithPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[3]/td[@data-label='My Business Details']/div/div/div/p/i

Ordering checked for BuyerWithoutPrice
    Selenium Library.Page Should Contain Element    xpath=//tbody[@role='rowgroup']/tr[4]/td[@data-label='Ordering']/div/div/div/p/i

Prices not checked for BuyerWithoutPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[4]/td[@data-label='Prices']/div/div/div/p/i

My Invoices not checked for BuyerWithoutPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[4]/td[@data-label='My Invoices']/div/div/div/p/i

My Business Details not checked for BuyerWithoutPrice
    Selenium Library.Page Should Not Contain Element    xpath=//tbody[@role='rowgroup']/tr[4]/td[@data-label='My Business Details']/div/div/div/p/i

Choose outlet
    Selenium Library.Wait until element is visible    xpath=//div[@class='ecp-table add-account']
    Sleep    5s
    Check available outlets
    Choose random outlet

Check available outlets
    ${outlets_count}    Selenium Library.Get Element Count    xpath=//tbody[@role='rowgroup']/tr
    Set global variable    ${outlets_count}

Choose random outlet
    ${random_int}    Evaluate    random.randint(1, ${outlets_count})    modules=random
    Set global variable    ${random_int}
    Execute Javascript    (function() {document.querySelector("tr:nth-child(${random_int}) .custom-control-label").click();})();

Remember chosen outlet
    ${chosen_outlet_name}    Selenium Library.Get text    css=tr:nth-child(${random_int}) > td:nth-child(2) span
    Set global variable    ${chosen_outlet_name}

Verify employee invited landing page
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center mb-5']    Employee invited!
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center mb-5']    Employee invited!
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center mb-5']    Votre collègue est invité!
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center mb-5']    Employee invited!
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Wait until element contains    xpath=//h3[@class='text-center mb-5']    Medewerker is uitgenodigd!


Click Go to My Employees button
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Click element    xpath=//button[contains(text(), 'Go to My Employees')]
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Click element    xpath=//button[contains(text(), 'Go to My Employees')]
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Click element    xpath=//button[contains(text(), 'Aller à Mes Utilisateurs')]
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Click element    xpath=//button[contains(text(), 'Go to My Employees')]
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Click element    xpath=//button[contains(text(), 'Ga naar Mijn Medewerkers')]
    Selenium library.Wait until element is visible    id=tab-myprofile
    Selenium Library.Wait until element is visible    id=tab-myemployees___BV_tab_button__

Remember employees status
    ${all_employees}    Selenium Library.Get text    xpath=//div[@id='tab-myemployees']/div[2]/div/div/div
    ${all_employees}    String.Fetch from right    ${all_employees}    (
    ${all_employees}    String.Fetch from left    ${all_employees}    )
    ${all_employees}    String.Strip string    ${all_employees}
    Set global variable    ${all_employees}
    ${all}    Selenium Library.Get text        xpath=//div/div/div/div/div/ul/li/a
    ${all}    String.Fetch from right    ${all}    (
    ${all}    String.Fetch from left    ${all}    )
    ${all}    String.Strip string    ${all}
    Set global variable    ${all}
    ${active}    Selenium Library.Get text    xpath=//div/div/div/div/ul/li[2]/a
    ${active}    String.Fetch from right    ${active}    (
    ${active}    String.Fetch from left    ${active}    )
    ${active}    String.Strip string    ${active}
    Set global variable    ${active}
    ${invited}    Selenium Library.Get text    xpath=//div/div/div/div/ul/li[3]/a
    ${invited}    String.Fetch from right    ${invited}    (
    ${invited}    String.Fetch from left    ${invited}    )
    ${invited}    String.Strip string    ${invited}
    Set global variable    ${invited}

Verify employees quantity has changed after invitation
    ${all_employees_before}    BuiltIn.Evaluate    ${all_employees_before} + 1
    ${all_before}    BuiltIn.Evaluate    ${all_before} + 1
    ${invited_before}    BuiltIn.Evaluate    ${invited_before} + 1
    BuiltIn.Should be equal as numbers    ${all_employees}    ${all_employees_before}
    BuiltIn.Should be equal as numbers    ${all}    ${all_before}
    BuiltIn.Should be equal as numbers    ${active}    ${active_before}
    BuiltIn.Should be equal as numbers    ${invited}    ${invited_before}

Verify employees quantity has changed after activation
    ${active_after}    BuiltIn.Evaluate    ${active_after} + 1
    ${invited_after}    BuiltIn.Evaluate    ${invited_after} - 1
    BuiltIn.Should be equal as numbers    ${all_employees}    ${all_employees_after}
    BuiltIn.Should be equal as numbers    ${all}    ${all_after}
    BuiltIn.Should be equal as numbers    ${active}    ${active_after}
    BuiltIn.Should be equal as numbers    ${invited}    ${invited_after}

Go to last page of My Employees table
    :FOR    ${i}    IN RANGE    100
    \    Is next page available
    \    Run keyword if    '${next_page_available}' == 'True'    Selenium Library.Click element    xpath=//div[contains(@data-testid, 'ccep-button-page-selector-next']/i[@class='fa fa-angle-right']
    \    Exit for loop if    '${next_page_available}' == 'False'

Is next page available
    ${next_page_available}    Run keyword and return status    Selenium Library.Element should be visible    xpath=//div[contains(@data-testid, 'ccep-button-page-selector-next']/i[@class='fa fa-angle-right']
    Set global variable    ${next_page_available}

Unfold pagination option for My Employees
    Selenium Library.Click Element    xpath=//div[2]/form/div/div[3]/div/button

Count employees rows
    ${employees_count}    Selenium Library.Get Element Count    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr
    Set global variable    ${employees_count}

Check the row of invited employee
    :FOR    ${i}    IN RANGE    1    ${employees_count}+1
    \    Set global variable   ${i}
    \    Get employee email
    \    Where is employee
    \    Exit for loop if    '${employee_row}' == 'True'
    Set global variable    ${i}

Get employee email
    Run keyword if    '${usr}' == 'GB'    Get employee email GB
    Run keyword if    '${usr}' == 'IBERIA'    Get employee email IBERIA
    Run keyword if    '${usr}' == 'BELUX'    Get employee email BELUX
    Run keyword if    '${usr}' == 'FRANCE'    Get employee email FRANCE
    Run keyword if    '${usr}' == 'NL'    Get employee email NL

Get employee email GB
    # this step in needed because employee is saved in random position in employees overview
    ${employee_email_row}    Selenium Library.Get text    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']
    Set global variable    ${employee_email_row}

Get employee email IBERIA
    # this step in needed because employee is saved in random position in employees overview
    ${employee_email_row}    Selenium Library.Get text    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']
    Set global variable    ${employee_email_row}

Get employee email BELUX
    # this step in needed because employee is saved in random position in employees overview
    ${employee_email_row}    Selenium Library.Get text    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='E-mail']
    Set global variable    ${employee_email_row}

Get employee email FRANCE
    # this step in needed because employee is saved in random position in employees overview
    ${employee_email_row}    Selenium Library.Get text    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']
    Set global variable    ${employee_email_row}

Get employee email NL
    # this step in needed because employee is saved in random position in employees overview
    ${employee_email_row}    Selenium Library.Get text    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='E-mail']
    Set global variable    ${employee_email_row}

Where is employee
    ${employee_row}    Run keyword and return status    BuiltIn.Should Be Equal As Strings    '${employee_email_row}'    '${invited_email}'
    Set global variable    ${employee_row}

Verify first name for invited employee present in overview table
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='First name']    QA INVITED
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='First name']    QA INVITED
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Prénom']    QA INVITED
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='First name']    QA INVITED
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Voornaam']    QA INVITED

Verify last name for invited employee present in overview table
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Last name']    ${usr} INVITED
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Last name']    ${usr} INVITED
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Nom']    ${usr} INVITED
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Last name']    ${usr} INVITED
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Achternaam']    ${usr} INVITED

Verify invited email present in overview table
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']    ${invited_email}
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']    ${invited_email}
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='E-mail']    ${invited_email}
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Email']    ${invited_email}
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='E-mail']    ${invited_email}

Verify role for invited employee present in overview table
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Role']    ${chosen_role_name}
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Role']    ${chosen_role_name}
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Rôle']    ${chosen_role_name}
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Role']    ${chosen_role_name}
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Rol']    ${chosen_role_name}

Verify added date for invited employee present in overview table
    ${current_date}    DateTime.Get current date    result_format=%d.%m.%Y
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Date added']    ${current_date}
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Date added']    ${current_date}
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[5]    ${current_date}
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Date added']    ${current_date}
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='ecp-table add-employee']/form/table/tbody/tr[${i}]/td[@data-label='Toegevoegd op']    ${current_date}

Open active employees tab
    Selenium Library.Click element    xpath=//div/div/div/div/ul/li[2]/a

Verify invited employee not in active employees
    Selenium Library.Page should not contain    ${invited_email}

Verify invited employee not in invited employees
    Selenium Library.Page should not contain    ${invited_email}

Open invited employees tab
    Selenium Library.Click element    xpath=//div/div/div/div/ul/li[3]/a

Open activate account link
    Selenium Library.Go to    ${site_url}/create-password/${registration_token}
    Selenium Library.Wait until element is visible    xpath=//h2[@class='text-center pwconfirm__title']

Verify activation button disabled
    Selenium Library.Element attribute value should be    xpath=//button[contains(@data-testid, 'ccep-button-create-password-send')]    disabled    true

Verify activation button enabled
    Selenium Library.Element attribute value should be    xpath=//button[contains(@data-testid, 'ccep-button-create-password-send')]    disabled    None

Enter activation password
    Selenium Library.Input text    id=ecp-input-create-password    ${ccep_password}

Enter activation password confirmation
    Selenium Library.Input text    id=ecp-input-create-password-retype    ${retype_password}
    #xpath=//label[contains(@data-testid, 'ccep-label-create-password-retype')]

Verify validation triggered for not matching password
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='validation-feedback text-left']/div    Passwords do not match
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Element text should be    xpath=//div[@class='validation-feedback text-left']/div    Passwords do not match
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Element text should be    xpath=//div[@class='validation-feedback text-left']/div    Votre mot de passe ne correspond pas
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='validation-feedback text-left']/div    Passwords do not match
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='validation-feedback text-left']/div    De ingevoerde wachtwoorden komen niet overeen

Verify no validation for not matching password
    Selenium Library.Element should not be visible    xpath=//div[@class='validation-feedback text-left']/div

Check terms of use checkbox
    Execute Javascript    (function() {document.querySelector(".mt-3 > .col-sm-12:nth-child(1) .custom-control-label").click();})();

Check privacy policy checkbox
    Execute Javascript    (function() {document.querySelector(".col-sm-12:nth-child(2) .custom-control-label").click();})();

Check conditions of sale checkbox
    Execute Javascript    (function() {document.querySelector(".col-sm-12:nth-child(3) .custom-control-label").click();})();

Submit activation form
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-button-create-password-send')]

Verify validation triggered for legal checkboxes
    Verify validation triggered for terms of use
    Verify validation triggered for privacy policy
    Verify validation triggered for conditions of sale

Verify validation triggered for terms of use
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${error_message}    This field is required
    Run keyword if    '${usr}' == 'IBERIA'    Set global variable    ${error_message}    This field is required
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${error_message}    Ce champ est obligatoire
    Run keyword if    '${usr}' == 'FRANCE'    Set global variable    ${error_message}    This field is required
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${error_message}    Dit is een verplicht veld
    Selenium Library.Element text should be    css=.col-sm-12:nth-child(1) > .validation-feedback > div    ${error_message}

Verify validation triggered for privacy policy
    Selenium Library.Element text should be    css=.col-sm-12:nth-child(2) > .validation-feedback > div    ${error_message}

Verify validation triggered for conditions of sale
    Selenium Library.Element text should be    css=.col-sm-12:nth-child(3) > .validation-feedback > div    ${error_message}

Verify employee activation succeeded
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Wait until element contains    xpath=//h2[@class='text-center']    Welcome!    60
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Wait until element contains    xpath=//h2[@class='text-center']    Welcome!    60
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Wait until element contains    xpath=//h2[@class='text-center']    Bienvenue!    60
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Wait until element contains    xpath=//h2[@class='text-center']    Welcome!    60
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Wait until element contains    xpath=//h2[@class='text-center']    Welkom!     60
    Selenium Library.Location Should Be    ${site_url}/create-password-ok

Go to dashboard from welcome page
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-submit-create-password-ok')]/div
    Sleep    10s

Verify duplicated invitation message
    Selenium Library.Element should contain    xpath=//div[@class='alert alert-primary']    There is already a user registered with this email address.

Verify back to MyProfile breadcrumb present
    Selenium Library.Element should be visible   xpath=//a[contains(@data-testid, 'ccep-a-breadcrumb-profile')]

Verify employee logged in
    #Selenium Library.Wait until element is visible
    Click Account icon
    Selenium Library.Element should be visible    xpath=//a[@href='/secur/logout.jsp']

Click Account icon
    Selenium Library.Click element    id=userDropdown

Verify MyProfile tabs visible
    Verify My profile tab visible
    Verify My accounts & employees tab visible
    Verify My business tab visible

Verify My profile tab visible
    Selenium library.Element should be visible    id=tab-myprofile___BV_tab_button__

Verify My accounts & employees tab visible
    Selenium Library.element should be visible    id=tab-myemployees___BV_tab_button__

Verify My business tab visible
    Selenium Library.Element should be visible    id=tab-mybusiness___BV_tab_button__

Click My profile tab
    Selenium library.Click element    id=tab-myprofile___BV_tab_button__

Click My accounts & employees tab
    Selenium Library.Click element    id=tab-myemployees___BV_tab_button__

Click My business tab
    Selenium Library.Click element    id=tab-mybusiness___BV_tab_button__

Verify User Information section visible
    Selenium Library.Element should be visible    xpath=//form[@class='profile-user']

Verify Password section visible
    Selenium Library.element should be visible    xpath=//form[2]/div/div[2]/div/div/div[@id='password']

Validate My profile tab sections visibility
    Verify User Information section visible
    Verify Password section visible

Validate My accounts & employees tab sections visibility
    Verify Accounts section visible
    Verify Employees section visible

Verify Accounts section visible
    Selenium Library.Element should be visible    xpath=//div[@class='ecp-table add-account']

Verify Employees section visible
    Selenium Library.Element should be visible    xpath=//div[@class='ecp-table add-employee']

Validate My business tab sections visibility
    Verify Business account information section visible

Verify Business account information section visible
    Selenium Library.Element should be visible    id=AccountInfo

Verify Personal Information display
    First name should be visible
    Last name should be visible
    Email should be visible
    #Date of birth should be visible
    Phone number should be visible
    #Mobile phone should be visible

Verify Personal Information updated
    Set test variable    ${sf_first_name}    ${first_name_updated}
    Set test variable    ${sf_last_name}    ${last_name_updated}
    First name should be visible
    Last name should be visible
    Email should be visible
    #Date of birth should be visible
    Phone number should be visible
    #Mobile phone should be visible

First name should be visible
    Get first name from Profile form
    BuiltIn.Should be equal as strings    ${first_name}    ${sf_first_name}

Get first name from Profile form
    ${first_name}    Selenium Library.Get text    css=.mt-4 > .profile-editable-field > dd
    Set global variable     ${first_name}

Last name should be visible
    Get last name from Profile form
    BuiltIn.Should be equal as strings    ${last_name}    ${sf_last_name}

Get last name from Profile form
    ${last_name}    Selenium Library.Get text    css=.col-lg-6:nth-child(2) dd
    Set global variable     ${last_name}

Email should be visible
    Get email from Profile form
    BuiltIn.Should be equal as strings    ${email}    ${sf_email}

Get email from Profile form
    ${email}    Selenium Library.Get text    css=.col-lg-6:nth-child(3) dd
    Set global variable     ${email}

#Date of birth should be visible

Phone number should be visible
    Get phone number from Profile form
    ${phone_number}    String.Replace string    ${phone_number}    ${SPACE}    ${EMPTY}
    BuiltIn.Should be equal as strings    ${phone_number}    ${sf_phone_number}

Get phone number from Profile form
    ${phone_number}    Selenium Library.Get text    css=.mb-0:nth-child(5) .view-value
    Set global variable     ${phone_number}

#Mobile phone should be visible

Verify Communication Information displayed
    Selenium Library.Page should contain element    xpath=//h4[contains(@data-testid, 'ccep-h4-accordion-Communicationpreferences')]
    Selenium Library.Element should be visible    css=.mt-4 > .profile-check > .profile-check__readonly
    Selenium Library.Element should be visible    css=.ml-4 > .profile-check:nth-child(1) > .profile-check__readonly
    Selenium Library.Element should be visible    css=.profile-check:nth-child(2) > .profile-check__readonly
    Selenium Library.Element should be visible    css=.profile-check:nth-child(3) > .profile-check__readonly

Edit profile
    Selenium Library.Click element    xpath=//div[contains(@data-testid, 'ccep-div-card-edit')]/span
    Selenium Library.Wait until element is visible    id=ecp-input-firstName

Cancel editing
    Selenium Library.Click element    xpath=//div[contains(@data-testid, 'ccep-div-card-edit')]/span
    Sleep    1s
    Selenium Library.Element should not be visible    id=ecp-input-firstName

Verify fields editable
    Selenium Library.Element should be visible    xpath=//input[@id='ecp-input-firstName']
    Selenium Library.Element should be visible    xpath=//input[@id='ecp-input-lastName']
    Selenium Library.Element should be visible    xpath=//input[@id='ecp-input-profile-birthdate']
    Selenium Library.Element should be visible    xpath=//input[@id='ecp-input-profile-phonenumber']
    Selenium Library.Element should be visible    xpath=//div[@class='input-field__wrapper']/label[contains(@data-testid, 'ccep-label-profile-phonenumber-mobile')]

Update first name
    ${first_name_updated}   Generate Random String    8    [LETTERS]
    Set global variable    ${first_name_updated}
    Selenium Library.Input text    id=ecp-input-firstName   ${first_name_updated}

Update last name
    ${last_name_updated}   Generate Random String    8    [LETTERS]
    Set global variable    ${last_name_updated}
    Selenium Library.Input text    id=ecp-input-lastName   ${last_name_updated}

Discard profile changes
    Selenium Library.Click element    xpath=//a[contains(@data-testid, 'ccep-a-profile-user-discard')]
    Selenium Library.Wait until element is not visible    id=ecp-input-firstName

Save profile changes
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-submit-profile-user-card')]/div
    Selenium Library.Wait until element is not visible    id=ecp-input-firstName

Verify edit password disabled
    Selenium Library.Element Attribute Value Should Be    xpath=//div[@id='tab-myprofile']/form[2]/div/div/div/div[2]/div[contains(@data-testid, 'ccep-div-card-edit')]    class    custom-header__action disabled

Clear mandatory profile data
    Selenium Library.Input text    id=ecp-input-firstName   ${EMPTY}
    Selenium Library.Input text    id=ecp-input-lastName   ${EMPTY}
    Selenium Library.Input text    id=ecp-input-profile-phonenumber    ${EMPTY}

Verify mandatory fields validation triggered
    Selenium Library.Element should be visible    css=.mt-4 .validation-feedback > div
    Selenium Library.Element should be visible    css=.col-lg-6:nth-child(2) .validation-feedback > div
    Selenium Library.Element should be visible    css=.input-phone__input .validation-feedback > div:nth-child(1)

Edit password
    Selenium Library.Click element    xpath=//div[@id='tab-myprofile']/form[2]/div/div/div/div[2]/div[contains(@data-testid, 'ccep-div-card-edit')]/span
    Selenium Library.Wait until element is visible    id=ecp-input-profile-password-old

Submit password form
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-submit-profile-password-card')]/div
    Sleep    8s

Verify mandatory password fields validation triggered
    Selenium Library.Element should be visible    css=.col-lg-3:nth-child(1) .validation-feedback > div
    Selenium Library.Element should be visible    css=#passwordNew .validation-feedback > div
    Selenium Library.Element should be visible    css=.col-lg-3:nth-child(3) .validation-feedback > div

Insert old password
    Selenium Library.Input text    id=ecp-input-profile-password-old    ${password}

Insert new password
    Selenium Library.Input text    id=ecp-input-profile-password-new    ${new_password}

Confirm new password
    Selenium Library.Input text    id=ecp-input-profile-password-verify    ${confirm_password}

Verify invalid old password not accepted
    Set test variable    ${password}    Incorrect123#
    Insert old password
    Set test variable    ${new_password}    NewPassword123#
    Insert new password
    Set test variable    ${confirm_password}     NewPassword123#
    Confirm new password
    Submit password form
    Verify old password invalid error message

Verify old password invalid error message
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${invalid_old_password_message}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'IBERIA'    Set global variable    ${invalid_old_password_message}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${invalid_old_password_message}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'FRANCE'    Set global variable    ${invalid_old_password_message}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${invalid_old_password_message}    Error: Your old password is invalid.
    Selenium Library.Element text should be    xpath=//div[@id='password']/div/div[6]/div    ${invalid_old_password_message}

Verify confirmation not matching new password not accepted
    Set test variable    ${password}    ${ccep_password}
    Insert old password
    Set test variable    ${new_password}    NewPassword123#
    Insert new password
    Set test variable    ${confirm_password}     NewPassword123##
    Confirm new password
    Submit password form
    Verify passwords do not match error message

Verify passwords do not match error message
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${passwords_not_matching}    Passwords do not match
    Run keyword if    '${usr}' == 'IBERIA'    Set global variable    ${passwords_not_matching}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${passwords_not_matching}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'FRANCE'    Set global variable    ${passwords_not_matching}    Error: Your old password is invalid.
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${passwords_not_matching}    Error: Your old password is invalid.
    Selenium Library.Element text should be    xpath=//div[@id='password']/div/div[3]/div/div[2]/div/div/div    ${passwords_not_matching}

Insert valid values and discard changes
    Set test variable    ${password}    ${ccep_password}
    Insert old password
    Set test variable    ${new_password}    NewPassword123#
    Insert new password
    Set test variable    ${confirm_password}     NewPassword123#
    Confirm new password
    Discard password changes
    Verify old password still valid

Discard password changes
    Selenium Library.Click element    xpath=//div[@id='password']/div/div[5]/div/a
    Selenium Library.Wait until element is not visible    id=ecp-input-profile-password-old

Verify old password still valid
    Log off
    Go to login page
    Log in to CCEP
    Verify user logged in

Insert valid values and submit changes
    Set test variable    ${password}    ${ccep_password}
    Insert old password
    Set global variable    ${new_password}    NewPassword123#
    Insert new password
    Set test variable    ${confirm_password}     NewPassword123#
    Confirm new password
    Submit password form
    Verify password form saved
    Verify new password valid

Verify password form saved
    Selenium Library.element should not be visible    id=ecp-input-profile-password-old

Verify new password valid
    Set test variable    ${ccep_password}    ${new_password}
    Log off
    Go to login page
    Log in to CCEP
    Verify user logged in

Old password cannot be used as new
    Set test variable    ${password}    ${new_password}
    Insert old password
    Set test variable    ${new_password}    ${new_password}
    Insert new password
    Set test variable    ${confirm_password}     ${new_password}
    Confirm new password
    Submit password form
    Verify old password cannot be used as new error message

Verify old password cannot be used as new error message
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${old_as_new_message}    Error: You cannot reuse this old password.
    Run keyword if    '${usr}' == 'IBERIA'    Set global variable    ${old_as_new_message}    Error: You cannot reuse this old password.
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${old_as_new_message}    Error: You cannot reuse this old password.
    Run keyword if    '${usr}' == 'FRANCE'    Set global variable    ${old_as_new_message}    Error: You cannot reuse this old password.
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${old_as_new_message}    Error: You cannot reuse this old password.
    Selenium Library.Element text should be    xpath=//div[@id='password']/div/div[6]/div    ${old_as_new_message}

Reset password to original value
    Go to MyProfile page
    Edit password
    Reset password to original

Reset password to original
    Set test variable    ${password}    ${new_password}
    Insert old password
    Set global variable    ${new_password}    ${original_password}
    Insert new password
    Set test variable    ${confirm_password}     ${original_password}
    Confirm new password
    Submit password form
    Verify password form saved

Verify password validation rules
    Selenium library.Set Focus To Element    id=ecp-input-profile-password-new
    Sleep    1s
    8 characters minimum not checked
    At least one uppercase letter not checked
    At least one lower case letter not checked
    At least one number not checked
    At least one special character not checked
    Set test variable    ${new_password}    a
    Insert new password
    8 characters minimum not checked
    At least one uppercase letter not checked
    At least one lower case letter checked
    At least one number not checked
    At least one special character not checked
    Set test variable    ${new_password}    aA
    Insert new password
    8 characters minimum not checked
    At least one uppercase letter checked
    At least one lower case letter checked
    At least one number not checked
    At least one special character not checked
    Set test variable    ${new_password}    aA1
    Insert new password
    8 characters minimum not checked
    At least one uppercase letter checked
    At least one lower case letter checked
    At least one number checked
    At least one special character not checked
    Set test variable    ${new_password}    aA1#
    Insert new password
    8 characters minimum not checked
    At least one uppercase letter checked
    At least one lower case letter checked
    At least one number checked
    At least one special character checked
    Set test variable    ${new_password}    123456789
    Insert new password
    8 characters minimum checked
    At least one uppercase letter not checked
    At least one lower case letter not checked
    At least one number checked
    At least one special character not checked
    Set test variable    ${new_password}    Suyo123#
    Insert new password
    Sleep    1s
    Selenium Library.Element should not be visible    xpath=//div[@class='popover-body']

8 characters minimum not checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[1]/i[@class='fa fa-times']

8 characters minimum checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[1]/i[@class='fa fa-check']

At least one uppercase letter not checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[2]/i[@class='fa fa-times']

At least one uppercase letter checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[2]/i[@class='fa fa-check']

At least one lower case letter not checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[3]/i[@class='fa fa-times']

At least one lower case letter checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[3]/i[@class='fa fa-check']

At least one number not checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[4]/i[@class='fa fa-times']

At least one number checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[4]/i[@class='fa fa-check']

At least one special character not checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[5]/i[@class='fa fa-times']

At least one special character checked
    Selenium Library.Element should be visible    xpath=//div[@class='popover-body']/div/div/ul/li[5]/i[@class='fa fa-check']
