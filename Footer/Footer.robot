*** Settings ***
Resource  keywords_footer.robot

**Test Cases**

Prerequisites
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Open CCEP

Validate legal links for not logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Verify data policy link visible
    Verify terms of use link visible
    Verify cookie policy link visible
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link visible
    Verify data policy link redirection
    Verify terms of use link redirection
    Verify cookie policy link redirection
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link redirection

Validate legal links for logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Go to login page
    Log in to CCEP
    Verify user logged in
    Verify data policy link visible
    Verify terms of use link visible
    Verify cookie policy link visible
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link visible
    Verify data policy link redirection
    Verify terms of use link redirection
    Verify cookie policy link redirection
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link redirection

Validate legal links on MyInvoices page
    [tags]    IBERIA    NL    FRANCE    GB
    Go to MyInvoices page
    Verify data policy link visible
    Verify terms of use link visible
    Verify cookie policy link visible
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link visible
    Verify data policy link redirection
    Verify terms of use link redirection
    Verify cookie policy link redirection
    Run keyword unless    '${usr}' == 'IBERIA'    Verify conditions of sale link redirection

CC footer logo validation
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    CC logo should be visible in footer
    CC logo should direct to ccep.com
    Close Browser
