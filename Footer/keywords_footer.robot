*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Verify data policy link visible
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@href, '/legal/data-policy')]

Verify terms of use link visible
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@href, '/legal/terms-of-use')]

Verify cookie policy link visible
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@href, '/legal/cookie')]

Verify conditions of sale link visible
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@href, '/legal/conditions-of-sale')]

Click data policy link
    Sleep    3s
    SeleniumLibrary.Click Element    xpath=//a[contains(@href, '/legal/data-policy')]
    Sleep    4s

Click terms of use link
    SeleniumLibrary.Click Element    xpath=//a[contains(@href, '/legal/terms-of-use')]
    Sleep    3s

Click cookie policy link
    SeleniumLibrary.Click Element    xpath=//a[contains(@href, '/legal/cookie')]
    Sleep    3s

Click conditions of sale link
    SeleniumLibrary.Click Element    xpath=//a[contains(@href, '/legal/conditions-of-sale')]
    Sleep    3s

Verify data policy link redirection
    Click data policy link
    SeleniumLibrary.Select Window    url=${site_url}/legal/data-policy
    Location Should Be    ${site_url}/legal/data-policy
    SeleniumLibrary.Close Window
    SeleniumLibrary.Select Window
    Sleep    3s

Verify terms of use link redirection
    Click terms of use link
    SeleniumLibrary.Select Window    url=${site_url}/legal/terms-of-use
    Location Should Be    ${site_url}/legal/terms-of-use
    SeleniumLibrary.Close Window
    SeleniumLibrary.Select Window
    Sleep    3s

Verify cookie policy link redirection
    Click cookie policy link
    SeleniumLibrary.Select Window    url=${site_url}/legal/cookie
    Location Should Be    ${site_url}/legal/cookie
    SeleniumLibrary.Close Window
    SeleniumLibrary.Select Window
    Sleep    3s

Verify conditions of sale link redirection
    Click conditions of sale link
    SeleniumLibrary.Select Window    url=${site_url}/legal/conditions-of-sale
    Location Should Be    ${site_url}/legal/conditions-of-sale
    SeleniumLibrary.Close Window
    SeleniumLibrary.Select Window
    Sleep    3s

CC logo should be visible in footer
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@data-testid, 'ccep-a-footer-home')]/img[contains(@alt, 'Coca-Cola European Partners')]

Click CC logo in footer
    SeleniumLibrary.Click Element    xpath=//img[@alt='Coca-Cola European Partners']

CC logo should direct to ccep.com
    Click CC logo in footer
    SeleniumLibrary.Location Should Be    ${site_url}/home
