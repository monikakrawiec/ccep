import math
import numbers

def round_up(int_value):
    return math.ceil(int_value)

def round_to_two_decimals(float_value):
    return round(float_value, 2)

def check_if_integer(float_value):
    return float(float_value).is_integer()
