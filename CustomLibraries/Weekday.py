from datetime import date
import calendar

def return_weekday(my_date):
    my_date = my_date.date()
    return calendar.day_name[my_date.weekday()]
