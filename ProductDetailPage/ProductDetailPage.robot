*** Settings ***
Resource  keywords_pdp.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Open detail page for random product
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Choose random product
    Verify PDP shown

Validate quantity picker
    [tags]    BELUX    NL    FRANCE    GB
    Set test variable    ${quantity_value}    0
    Insert quantity value
    Verify input field highlighted with error
    Verify add to cart button disabled
    Verify quantity increase disabled
    Verify quantity decrease disabled
    Set global variable    ${quantity_value}    1
    Insert quantity value
    Verify input field not highlighted with error
    Verify add to cart button enabled
    Verify quantity increase enabled
    Verify quantity decrease disabled
    Set global variable    ${quantity_value}    2    # will be needed in following test case
    Insert quantity value
    Verify input field not highlighted with error
    Verify quantity increase enabled
    Verify quantity decrease enabled

Add to cart product in some quantity
    [tags]    Belux    Nl    GB
    Verify add to cart button updated with quantity
    Add to cart
    Verify add to cart button after addition

#Case unit chosen by default
    #Close browser
