*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Verify PDP shown
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'product-detail view container')]

Verify add to cart button disabled
    SeleniumLibrary.Element should be visible    xpath=//button[contains(@class, 'btn btn-primary btn-block disabled')]

Verify add to cart button enabled
    SeleniumLibrary.Element should not be visible    xpath=//button[contains(@class, 'btn btn-primary btn-block disabled')]
    SeleniumLibrary.Element should be visible    xpath=//button[contains(@class, 'btn btn-primary btn-block')]

Increase quantity
    SeleniumLibrary.Click Element    xpath=//button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3')]

Add to cart
    #Execute Javascript    function myFunction() {document.getElementsByClassName("d-none d-sm-inline").click();}
    #${v_position}    Get Vertical Position    xpath=//div[contains(@class, 'product-detail view container')]/div[2]/div[2]/div[3]/div[3]/button/span
    #${v_position}    Convert To Integer    ${v_position}
    #${h_position}    Get Horizontal Position    xpath=//div[contains(@class, 'product-detail view container')]/div[2]/div[2]/div[3]/div[3]/button/span
    #${h_position}    Convert To Integer    ${h_position}
    #Click Element At Coordinates    xpath=//div[contains(@class, 'product-detail view container')]/div[2]/div[2]/div[3]/div[3]/button/span    ${v_position}    ${h_position}
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'product-detail view container')]/div[2]/div[2]/div[3]/div[3]/button/span
    Sleep    10s

Verify add to cart button updated with quantity
    Get quantity from button
    Should be equal as strings    ${quantity_button}    ${quantity_value}

Get quantity from button
    ${quantity_button}    Selenium Library.Get Text      css=.btn > span
    @{words}    String.Split String    ${quantity_button}    ${SPACE}
    Set global variable    ${quantity_button}    @{words}[-2]

Verify add to cart button after addition
    Get quantity from button
    Should be equal as strings    ${quantity_button}    1

Increase quantity from PDP
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[2]

Set quantity
    Press Key    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input    \\8
    SeleniumLibrary.Input Text    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input    ${quantity_value}

Choose palet option
    SeleniumLibrary.Click Element    xpath=//button[contains(@data-testid, 'ccep-button-unit-switch-PAL')]

Choose Layer unit
    Selenium Library.Click element    xpath=//div[@class='unit-switch row']/div[2]/button

Get palletisation info PDP
    ${palletisation_info_pdp}    Selenium Library.Get text    xpath=//div[@class='product-detail__numerator']
    @{words}    String.Split String    ${palletisation_info_pdp}    /
    Set global variable    ${cases_per_layer}    @{words}[0]
    ${cases_per_layer}    String.Fetch from left    ${cases_per_layer}    ${SPACE}
    ${cases_per_layer}    BuiltIn.Convert to integer    ${cases_per_layer}
    Set global variable    ${cases_per_layer}
    Set global variable    ${cases_per_palet}    @{words}[1]
    ${cases_per_palet}    String.Strip String    ${cases_per_palet}
    ${cases_per_palet}    String.Fetch from left    ${cases_per_palet}    ${SPACE}
    ${cases_per_palet}    BuiltIn.Convert to integer    ${cases_per_palet}
    Set global variable    ${cases_per_palet}
    ${layer_per_palet}    BuiltIn.Evaluate    ${cases_per_palet}/${cases_per_layer}
    ${layer_per_palet}    MathOperations.Round up    ${layer_per_palet}
    ${layer_per_palet}    BuiltIn.Convert to integer    ${layer_per_palet}
    Set global variable    ${layer_per_palet}
    Set global variable      ${palletisation_info_pdp}

Verify expected unit highlighted
    ${expected unit}    Selenium Library.Get Text    xpath=//button[@class='btn unit-switch__unit btn-block btn-primary']
    BuiltIn.Should be equal as strings    ${expected unit}    ${pattern_unit}
