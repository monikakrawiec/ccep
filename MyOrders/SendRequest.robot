*** Settings ***
Resource          keywords_myorders.robot

*** Test Cases ***

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Make a successful order
    [tags]    BELUX    NL    FRANCE    GB
    Run keyword if    '${usr}' == 'BELUX'    Belux set locale to FR
    Show all products
    Choose random product
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${quantity_value}    150
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${quantity_value}    3000
    Set quantity
    Add to cart
    Click cart icon
    Process to checkout
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number

Send a request corresponding to order
    [tags]    BELUX    NL    FRANCE    GB
    Choose ShopProducts from top menu
    Choose MyOrders from top menu
    Unfold order actions button
    Open request form
    Validate request form mandatory fields
    Choose request reason
    Remember chosen reason
    Insert request description
    Submit request
    Verify request form submitted

Validate case in SalesForce
    [tags]    BELUX    NL    FRANCE    GB
    Go to Salesforce
    Log in to SalesForce
    Open SalesForce classic
    Show all tabs
    Go to cases
    Select all cases
    #Select all ECP cases
    #Show latest cases on top
    #Choose first case from list
    Run keyword if    '${usr}' == 'GB'    Map request with salesforce category GB
    Run keyword if    '${usr}' == 'BELUX'    Map request with salesforce category BELUX
