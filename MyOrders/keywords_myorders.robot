*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

No action shown by default
     Selenium Library.Element text should be    xpath=//div[3]/div/div[contains(@class, 'dropdown btn-group b-dropdown')]/button    ECP_Ordering_ChooseAction

Unfold order actions button
    Selenium Library.Click element    xpath=//div[@class='order-actions']/div[2]/div/button

Actions should be organised in dropdown menu
    Selenium Library.Element should be visible    xpath=//div[3]/div/div[contains(@class, 'dropdown btn-group b-dropdown')]/button
    #xpath=//div[3]/div/div/button
    #xpath=//div[@id='__BVID__1019']/button

Verify ordering request action
    Unfold order actions button
    Choose ordering request action
    Send request modal should be opened

Choose ordering request action
    Selenium Library.Click link    link=ECP_Ordering_Request_Button
    Selenium Library.Wait until element is visible    id=MODAL_ORDER_REQUEST___BV_modal_content_

Send request modal should be opened
    Selenium Library.Element should be visible    id=MODAL_ORDER_REQUEST___BV_modal_content_

Close request modal
    Selenium Library.Click Element    xpath=//a[@data-testid='ccep-a-modal-close-MODALORDERREQUEST-3']/i

Click reorder button for chosen order
    Selenium Library.Click Element    xpath=//button[contains(@data-testid, 'ccep-submit-reorder']
    Selenium Library.Wait until element is visible    id=MODAL_REORDER_CONFIRM___BV_modal_content_

Reorder last order
    Selenium Library.Click element    xpath=//button[@data-testid='ccep-submit-reorder']/div
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-button-step-cart-next')]

Confirm reorder
    Selenium Library.Click element    xpath=//button[contains (data-testid, 'ccep-submit-reorder-confirm')]

Cancel last reorder
    Unfold order actions button
    Selenium Library.Click element    xpath=//a[@data-testid='ccep-dropdown--option-1']

Confirm order cancelation
    Selenium Library.Click Element    xpath=//button[@data-testid='ccep-submit-order-cancel-confirm']/div

Verify no retract option available
    Unfold order actions button
    Verify no retract option visible

Verify retract option available
    Unfold order actions button
    Verify retract option visible

Verify no retract option visible
    Selenium Library.Element should not be visible    link=Retract order

Verify retract option visible
    Selenium Library.Element should be visible    link=Retract order

Retract last order
    Selenium Library.Click link    link=Retract order
    Selenium Library.Wait until element is visible    id=MODAL_ORDER_CANCEL_CONFIRM___BV_modal_header_

Confirm retract
    Selenium Library.Click element    xpath=//button[@data-testid='ccep-submit-order-cancel-confirm']
    Selenium Library.Wait until element is not visible    id=MODAL_ORDER_CANCEL_CONFIRM___BV_modal_header_

Verify order retracted
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'FRANCE'    Selenium Library.Element text should be    xpath=//div[@class='orders']/div[2]/div[1]/div/div/div/div[@class='row']/div[2]/p[1]/span    CANCELLED
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Element text should be    xpath=//div[@class='orders']/div[2]/div[1]/div/div/div/div[@class='row']/div[2]/p[1]/span    GEANNULEERD
    Run keyword if    '${usr}' == 'BElUX'    Selenium Library.Element text should be    xpath=//div[@class='orders']/div[2]/div[1]/div/div/div/div[@class='row']/div[2]/p[1]/span    Annulée

Open request form
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Click link    link=Send request
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Click link    link=Faire une demande
    Selenium Library.Wait until element is visible    id=MODAL_ORDER_REQUEST___BV_modal_content_

Choose request reason
    Unfold request reason list
    Count request reasons
    Choose random reason

Choose random reason
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Click element    css=.show > li > .dropdown-item
    Run keyword unless   '${usr}' == 'GB'    Random reason from list

Random reason from list
    ${random_int}    BuiltIn.Evaluate    random.randint(1, ${request_reasons_count})    modules=random
    Selenium Library.Click element    css=.show > li:nth-child(${random_int}) > .dropdown-item

Remember chosen reason
    ${chosen_reason}    Selenium Library.Get text    xpath=//div[contains(@data-testid, 'ccep-dropdown-order-request-reason')]/button
    Set global variable    ${chosen_reason}

Count request reasons
    ${request_reasons_count}    Selenium Library.Get Element Count    xpath=//ul[@class='dropdown-menu show']/li
    Set global variable    ${request_reasons_count}

Unfold request reason list
    Selenium Library.Click element    xpath=//div[contains(@data-testid, 'ccep-dropdown-order-request-reason')]/button
    #Selenium Library.Wait until element is visible    xpath=//a[@class='dropdown-item']

Insert request description
    Selenium Library.Input text    id=ecp-input-order-request-description    ${order_number} request description

Submit request
    Selenium Library.Click element    xpath=//button[@data-testid='ccep-submit-order-request-modal']/div

Verify request form submitted
    Selenium Library.Wait until element is not visible    id='MODAL_ORDER_REQUEST___BV_modal_content_'
    Selenium Library.Wait until element is visible    css=#MODAL_INVOICE_SUCCESS___BV_modal_header_ > .modal-title

Validate request form mandatory fields
    Submit request
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${required_field_message}    This field is required
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${required_field_message}    Ce champ est obligatoire
    Selenium Library.Element text should be    xpath=//div[@class='ecp-dropdown form-group form-required']/div[@class='validation-feedback text-left']/div    ${required_field_message}
    Selenium Library.Element text should be    xpath=//div[@class='row input-textarea__status']/div/div[@class='validation-feedback text-left']/div    ${required_field_message}
