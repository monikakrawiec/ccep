*** Settings ***
Resource          keywords_myorders.robot

*** Test Cases ***

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Make an order with delivery within 5 days before cut-off
    [tags]    BELUX    NL    FRANCE    GB
    Run keyword if    '${usr}' == 'BELUX'    Belux set locale to FR
    Set delivery date to next available date
    Show all products
    Choose random product
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${quantity_value}    150
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${quantity_value}    3000
    Run keyword unless    '${usr}' == 'FRANCE'    Set quantity
    Add to cart
    Click cart icon
    Process to checkout
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
    Choose MyOrders from top menu
    Verify no retract option available

Make an order with delivery longer than 5 days before cut-off
    [tags]    BELUX    NL    FRANCE    GB
    Choose Dashboard from top menu
    Set delivery date to last available date
    Show all products
    Choose random product
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${quantity_value}    150
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${quantity_value}    3000
    Run keyword unless    '${usr}' == 'FRANCE'    Set quantity
    Add to cart
    Click cart icon
    Remember basket items
    Set global variable    @{cart_products_order}    @{cart_products}
    Process to checkout
    Remember checkout items
    Remember items quantity
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
    Remember delivery date
    Choose MyOrders from top menu
    Verify retract option available

Retract order
    [tags]    BELUX    NL    FRANCE    GB
    Retract last order
    Confirm retract
    Verify order retracted
    Verify no retract option available

Reorder retracted order
    [tags]    BELUX    NL    FRANCE    GB
    Choose MyOrders from top menu
    Reorder last order
    #Confirm reorder
    Remember basket items
    Set global variable    @{cart_products_reorder}    @{cart_products}
    Verify all products added to cart from original order
    Process to checkout
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
