*** Settings ***
Resource          keywords_myorders.robot

*** Test Cases ***

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in
    Choose MyOrders from top menu

Validate dropdown actions
    [tags]    BELUX    NL    FRANCE    GB
    Actions should be organised in dropdown menu
    No action shown by default
    Verify ordering request action
    Close request modal
