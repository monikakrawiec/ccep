*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Select Terms&conditions checkbox test
    Execute Javascript    (function() {document.querySelector("#app > main > section > div > div > form > div:nth-child(3) > div:nth-child(2) > div > div > label").click();})();
    #Click Element    css=.custom-control-label
    #${v_position}    Get Vertical Position    css=.custom-control-label
    #${v_position}    Convert To Integer    ${v_position}
    #${h_position}    Get Horizontal Position    css=.custom-control-label
    #${h_position}    Convert To Integer    ${h_position}
    #Click Element At Coordinates    css=.custom-control-label    ${v_position}    ${h_position}
    #Click Element    css=.custom-control-label
    #Click Element At Coordinates    css=#checkout__terms    ${v_position}    ${h_position}
    #Execute Javascript    function myFunction() {document.elementFromPoint(${h_position}, ${v_position}).click();}
    #Execute Javascript    function myFunction() {document.elementFromPoint(${v_position}, ${h_position}).click();}
    #Click Element At Coordinates    css=.custom-control-label    ${h_position}    ${v_position}
    #Mouse Over    css=.custom-control-label
    #Sleep    5s
    #Mouse Down On Link    css=.custom-control-label
    #Sleep    5s
    #Click Element At Coordinates    css=.custom-control-label    1684    1130
    #Click Element    css=.custom-control
    #Execute Javascript    js.executeScript("window.document.getElementById('gbqfb').click()");
    #Execute Javascript    (function(){$('#agreement .terms_agreed input').prop("checked",true);})();
    #${Element}    Get WebElement    xpath=//label[@class='custom-control-label']
    #Execute JavaScript    click(),${Element}
    #Execute JavaScript    document.getElementById('checkout__terms').onclick()
    #Execute Javascript    $('#checkout__terms').click();
    #Execute Javascript    (function(){$('#checkout__terms').onclick();})();
    #Execute Javascript    function myFunction() {document.getElementById("checkout__terms").click();}
    Execute Javascript    function myFunction() {document.getElementsByClassName("custom-control-label").click();}
    #Execute Javascript    function myFunction() {document.getElementsByClassName("custom-control-label").click();}
    #Click element    xpath=//label[contains(.,'I accept the conditions of sale.')]
    #Sleep    3s
    #${terms_link}    Get Text    //*[@id="app"]/main/section/div/div/form/div[3]/div[2]/div/div/label/div/p
    #Click Element    //*[@id="app"]/main/section/div/div/form/div[3]/div[2]/div/div/label/div/p
    #Click element    //*[@id="app"]/main/section/div/div/form/div[3]/div[2]/div/div/label/div/p/text()[1]
    #//*[@id="app"]/main/section/div/div/form/div[3]/div[2]/div/div/label/div/p
    #Click Element    xpath=//p[contains(.,'I accept the ')]
    #Select Checkbox    id=checkout__terms
    #Execute Javascript    function myFunction() {document.querySelector("#app > main > section > div > div > form > div:nth-child(3) > div:nth-child(2) > div > div > label > div > p").innerHTML = "New Text";}
    #Execute Javascript    function myFunction() { document.getElementsByClassName("text-right checkout__terms-content").innerHTML = "New Text";}
