*** Settings ***
Resource  keywords_test.robot

**Test Cases**

Prerequisites
    [tags]    Iberia    Belux    Nl    France    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in
    Choose MyInvoices from top menu
