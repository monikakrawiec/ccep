*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Click cart icon
    SeleniumLibrary.Click Element    css=.cart__badge

Go to cart
    SeleniumLibrary.Click Element    xpath=//a[contains(@href, '/shop/cart')]

Remove all products from basket
    Click clear cart button
    Approve deletion

Click clear cart button
    SeleniumLibrary.Click Element    id=clear-cart-btn

Approve deletion
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .pb-2 span

Cancel deletion
    SeleniumLibrary.Click Element    css=div:nth-child(1) > div:nth-child(2) > .btn-link

Verify basket empty
    Check basket items
    Should be equal as numbers    ${basket_items}    0

Check basket items
    ${basket_items}    SeleniumLibrary.Get Text    xpath=//span[contains(@class, 'cart__badge cart__badge--primary cart__badge--circle')]
    ${basket_items}    String.Fetch From Left   ${basket_items}    +
    Set global variable    ${basket_items}

Remember basket items
    Get products count from cart
    @{cart_products}    Create List
    :FOR    ${i}    IN RANGE    2    ${cart_products_count}+1    # counts from 2 because first div is a container title
    \    ${name}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'cart')]/div[contains(@class, 'row')][${i}]/div/div[1]/div/div[1]/div[1]/span
    \    Collections.Append To List    ${cart_products}    ${name}
    Set Global Variable    @{cart_products}

Get products count from cart
    ${cart_products_count}    SeleniumLibrary.Get Element Count    xpath=//div[contains(@class, 'cart')]/div
    Set Global Variable    ${cart_products_count}

Remove first item from cart
    SeleniumLibrary.Click Element    xpath=//i[contains(@class, 'fa fa-trash-o')]
    Confirm deletion
    Sleep    3s

Confirm deletion
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .pb-2 span

Browse more products
    #Run keyword unless    '${usr}' == 'GB' and '${env}' == 'ecpt'    SeleniumLibrary.Click Element    css=.col-6:nth-child(1) > .btn
    #Run keyword if    '${usr}' == 'GB' and '${env}' == 'ecpt'    SeleniumLibrary.Click Element    css=.col-xl-4:nth-child(1) > .btn
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'row justify-content-between row-buttons')]/div[1]/button
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'section ecp-shop shop view container')]
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'products-overview__header-count')]

Verify product added to cart
    SeleniumLibrary.Element Text Should Be    xpath=//span[contains(@class, 'product-name')]    ${product_name}

Process to checkout
    Sleep    5s
    SeleniumLibrary.Click Element    css=.col-sm-12 > button:nth-child(1)
    Selenium Library.Wait until element is visible    xpath=//a[contains(@data-testid, 'ccep-a-step-card-edit')]
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-submit-order-complete')]
    Selenium Library.Wait until element is visible    xpath=//div[@class='productlist-item__roundings--bold']
    Selenium Library.Wait until page contains element    xpath=//button[contains(@data-testid, 'ccep-submit-order-complete')]
    Sleep    5s

Validate checkout button grayed out
    Run keyword unless    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'row justify-content-between row-buttons')]/div[3]/button[contains(@disabled, 'disabled')]

Remember item quantity
    ${item_quantity}    SeleniumLibrary.Get Text     css=.ecp-quantity > .col-6
    Set global variable    ${item_quantity}

Increase item quantity
    SeleniumLibrary.Click Element    css=.ecp-quantity__action-increase

Verify blocked ordering message appears
    SeleniumLibrary.Element Should Be Visible    id=MODAL_BLOCKED_ORDERING___BV_modal_content_

Verify checkout button not present
    SeleniumLibrary.Element should not be visible    css=.col-sm-12 > button:nth-child(1)

Verify clear cart button not present
    SeleniumLibrary.Element should not be visible    id=clear-cart-btn

Verify browse more button present
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'row justify-content-between row-buttons')]/div[1]/button

Insert quantity value
     Press Key    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input    \\8
     Press Key    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input    \\8
     #SeleniumLibrary.Clear Element Text    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input
     SeleniumLibrary.Input Text    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input    ${quantity_value}

Verify input field highlighted with error
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input[contains(@class, 'col-6 ecp-quantity--input-error')]

Verify input field not highlighted with error
    SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/input[contains(@class, 'col-6 ecp-quantity--input-error')]

Verify quantity increase disabled
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3 disabled')]

Verify quantity decrease disabled
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3 disabled')]

Verify quantity increase enabled
    SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3 disabled')]
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3')]

Verify quantity decrease enabled
    SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3 disabled')]
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'ecp-quantity row btn-group')]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3')]

Verify error message for unit minimum quantity
    Selenium Library.Element text should be    xpath=//div[contains(@class, 'row alert-on-top')]/div    You do not comply with our minimum order quantity of ${case_required} Case in total. Please add ${case_missing} more Case to you shopping basket.

Verify error message for palet minimum quantity
    Selenium Library.Element text should be    xpath=//div[contains(@class, 'row alert-on-top')]/div    You do not comply with our minimum order quantity of ${palet_required} Pallet in total. Please add ${palet_missing} more Pallet to you shopping basket.

Calculate quantity in cases
    ${quantity_in_cases}    BuiltIn.Evaluate    ${quantity_value} * ${cases_per_layer}   # for error message validation
    Set global variable    ${quantity_in_cases}

Calculate missing palets NL
    ${palet_missing}    BuiltIn.Evaluate    (${palet_required} * ${cases_per_palet} - ${quantity_in_cases}) / ${cases_per_palet}.0
    ${palet_missing}    MathOperations.Round to two decimals    ${palet_missing}
    Set global variable    ${palet_missing}
    ${is_integer}    MathOperations.Check if integer    ${palet_missing}
    Run keyword if    '${is_integer}' == 'True'    Convert float to integer
    Set global variable    ${palet_missing}

Convert float to integer
    ${palet_missing}    BuiltIn.Convert to integer    ${palet_missing}
    Set global variable    ${palet_missing}

Verify error message for palet maximum quantity
    Selenium Library.Element text should be    xpath=//div[contains(@class, 'row alert-on-top')]/div    You have exceeded the maximum quantity. You can use a maximum of ${palet_required} Palet(s).

Verify process to checkout button disabled
    Selenium Library.Element should be visible    xpath=//div[@class='row justify-content-between row-buttons']/div[3]/button[@disabled='disabled']

Save cart changes
    Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-button-step-cart-save')]
    Selenium Library.Wait until element is visible    xpath=//div[@class='row justify-content-between row-buttons']/div[3]/button[@disabled='disabled']
    Sleep    8s

Verify no error message for unit minimum quantity
    Selenium Library.Element should not be visible    xpath=//div[contains(@class, 'row alert-on-top')]/div

Verify process to checkout button enabled
    Selenium Library.Element should not be visible    xpath=//div[@class='row justify-content-between row-buttons']/div[3]/button[@disabled='disabled']

Set how many case required
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${case_required}    150
    Run keyword if    '${usr}' == 'BELUX'    Set global variable    ${case_required}    15

Set how many palet required
    Run keyword if    '${usr}' == 'GB'    Set global variable    ${palet_required}    26
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${palet_required}    4

Set how many layer required
    Set how many case required
    ${layer_required}    BuiltIn.Evaluate    ${case_required} / ${cases_per_layer}.0
    ${layer_required}    MathOperations.Round up    ${layer_required}
    ${layer_required}    BuiltIn.Convert to integer    ${layer_required}
    Set global variable    ${layer_required}

Get palletisation info cart
    ${palletisation_info_cart}    Selenium Library.Get text    xpath=//span[@class='product-palletisation']
    Set global variable    ${palletisation_info_cart}
