*** Settings ***
Resource  keywords_cart.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Verify cart not accessible for FR user
    #[tags]    FRANCE
    Click cart icon
    Verify blocked ordering message appears
    Close browser

Validate empty cart
    [tags]    BELUX    NL    FRANCE    GB
    Click cart icon
    Check basket items
    Run keyword if    ${basket_items} > 0    Remove all products from basket
    Verify checkout button not present
    Verify clear cart button not present
    Verify browse more button present
    Selenium Library.Reload page
    Verify checkout button not present
    Verify clear cart button not present
    Verify browse more button present

Add to cart from PDP
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Choose random product
    Set suite variable    ${random_int_first}    ${random_int}
    Add to cart
    Click cart icon
    Verify product added to cart

Remove 1 item from basket
    [tags]    BELUX    NL    FRANCE    GB
    Browse more products
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}+1      #this loop is created to avoid situation that 2 the same products will be added
    \    Choose random product
    \    Exit for loop if    ${random_int_first} != ${random_int}
    \    Show all products
    Add to cart
    Click cart icon
    Remember basket items
    Set suite variable    @{cart_products_before_removal}    @{cart_products}
    Remove first item from cart
    Remember basket items
    Should be equal as numbers    ${cart_products_count}    2    # only 1 item in cart (there are 2 rows including header)
    Should be equal as strings    @{cart_products_before_removal}[1]    @{cart_products}[0]

Cancel products deletion
    [tags]    BELUX    NL    FRANCE    GB
    Remember basket items
    Set suite variable    @{cart_products_before_removal}    @{cart_products}
    Click clear cart button
    Cancel deletion
    Should be equal as strings    @{cart_products_before_removal}    @{cart_products}

Validate not sufficient basket value to proceed to checkout
    [tags]    BELUX    NL    GB
    Validate checkout button grayed out

Verify quantity change not possible for invalid input
    [tags]    BELUX    NL    FRANCE    GB
    Set test variable    ${quantity_value}    0
    Insert quantity value
    Verify input field highlighted with error
    Verify quantity increase disabled
    Verify quantity decrease disabled
    Set test variable    ${quantity_value}    1
    Insert quantity value
    Verify input field not highlighted with error
    Verify quantity increase enabled
    Verify quantity decrease disabled
    Set test variable    ${quantity_value}    2
    Insert quantity value
    Verify input field not highlighted with error
    Verify quantity increase enabled
    Verify quantity decrease enabled

Cart emptied when updating outlet in dashboard
    [tags]    GB
    Check basket items
    BuiltIn.Should be equal as numbers    ${basket_items}    1
    Choose Dashboard from top menu
    Edit outlet widget
    Verify cart deletion message appears
    Confirm outlet update
    Check basket items
    BuiltIn.Should be equal as numbers    ${basket_items}    0

Verify Case unit validation
    [tags]    GB
    Set delivery to MDT
    Show all products
    Set how many case required
    Set global variable    ${start_point}    1
    Choose buyable product with Case unit
    Add to cart
    Click cart icon
    Set global variable    ${current_case quantity}    1
    ${case_missing}    BuiltIn.Evaluate    ${case_required} - ${current_case quantity}
    Set global variable    ${case_missing}
    Verify error message for unit minimum quantity
    Verify process to checkout button disabled
    Increase item quantity
    Increase item quantity
    Save cart changes
    Set global variable    ${current_case quantity}    3
    ${case_missing}    BuiltIn.Evaluate    ${case_required} - ${current_case quantity}
    Set global variable    ${case_missing}
    Verify error message for unit minimum quantity
    Verify process to checkout button disabled
    Browse more products
    ${start_point}    BuiltIn.Evaluate    ${i}+1    #    to avoid the same product is being chosen
    Set global variable    ${start_point}
    Choose buyable product with Case unit
    Add to cart
    Click cart icon
    Set global variable    ${current_case quantity}    4
    ${case_missing}    BuiltIn.Evaluate    ${case_required} - ${current_case quantity}
    Set global variable    ${case_missing}
    Verify error message for unit minimum quantity
    Verify process to checkout button disabled
    Browse more products
    ${start_point}    BuiltIn.Evaluate    ${i}+1    #    to avoid the same product is being chosen
    Set global variable    ${start_point}
    Choose buyable product with Case unit
    Set global variable    ${quantity_value}    ${case_missing}
    Set quantity
    Add to cart
    Click cart icon
    Verify no error message for unit minimum quantity
    Verify process to checkout button enabled
    Remove all products from basket

Verify Layer unit validation
    [tags]    GB
    Set delivery to MDT
    Show all products
    Choose buyable product with Layer unit
    Choose Layer unit
    Get palletisation info PDP
    Add to cart
    Click cart icon
    Set how many layer required
    ${quantity_value}    BuiltIn.Evaluate    ${layer_required}-1
    Set global variable    ${quantity_value}
    Insert quantity value
    Save cart changes
    ${case_missing}    BuiltIn.Evaluate    ${case_required} - ${quantity_value} * ${cases_per_layer}
    Set global variable    ${case_missing}
    Verify error message for unit minimum quantity
    Verify process to checkout button disabled
    Set global variable    ${quantity_value}    ${layer_required}
    Insert quantity value
    Save cart changes
    Verify no error message for unit minimum quantity
    Verify process to checkout button enabled

Verify Palet unit validation
    [tags]    GB
    Remove all products from basket
    Set delivery to Bulk
    Show all products
    Choose random product
    Get palletisation info PDP
    Add to cart
    Click cart icon
    Set how many palet required
    ${quantity_value}    BuiltIn.Evaluate    ${palet_required}-1
    Set global variable    ${quantity_value}
    Insert quantity value
    Save cart changes
    ${palet_missing}    BuiltIn.Evaluate    ${palet_required} - ${quantity_value}
    Set global variable    ${palet_missing}
    Verify error message for palet minimum quantity
    Verify process to checkout button enabled
    Set global variable    ${quantity_value}    ${palet_required}
    Insert quantity value
    Save cart changes
    Verify no error message for unit minimum quantity
    Verify process to checkout button enabled
    ${quantity_value}    BuiltIn.Evaluate    ${palet_required}+1
    Set global variable    ${quantity_value}
    Insert quantity value
    Save cart changes
    Verify error message for palet maximum quantity
    Verify process to checkout button enabled

Validate BELUX unit changes when reaching minimum quantity
    [tags]    BELUX
    Click cart icon
    Get products count from cart
    Run keyword if    ${cart_products_count} > 1    Remove all products from basket
    Show all products
    Choose buyable product with Unite unit
    Set global variable    ${pattern_unit}    Unité(s)
    Verify expected unit highlighted
    Get palletisation info PDP
    Add to cart
    Click cart icon
    Verify expected unit highlighted
    ${quantity_value}    BuiltIn.Evaluate    ${cases_per_layer}-1
    Set global variable    ${quantity_value}
    Insert quantity value
    Save cart changes
    Verify expected unit highlighted
    Process to checkout
    Get products count from checkout
    Remember items quantity
    Collections.List should contain value    ${checkout_items_quantity}    ${quantity_value} unité
    Edit cart
    Set global variable    ${quantity_value}    ${cases_per_layer}
    Insert quantity value
    Save cart changes
    Set global variable    ${pattern_unit}    Cou
    Verify expected unit highlighted
    Set global variable    ${quantity_value}    1
    Process to checkout
    Get products count from checkout
    Remember items quantity
    Collections.List should contain value    ${checkout_items_quantity}    ${quantity_value} couche(s)
    Edit cart
    ${quantity_value}    BuiltIn.Evaluate    ${layer_per_palet}-1
    Set global variable    ${quantity_value}
    Insert quantity value
    Save cart changes
    Verify expected unit highlighted
    Process to checkout
    Get products count from checkout
    Remember items quantity
    Collections.List should contain value    ${checkout_items_quantity}    ${quantity_value} couche(s)
    Edit cart
    Set global variable    ${quantity_value}    ${layer_per_palet}
    Insert quantity value
    Save cart changes
    Set global variable    ${pattern_unit}    Pal
    Verify expected unit highlighted
    Set global variable    ${quantity_value}    1
    Process to checkout
    Get products count from checkout
    Remember items quantity
    Collections.List should contain value    ${checkout_items_quantity}    ${quantity_value} palette(s)

Validate NL unit changes when reaching minimum quantity
    [tags]    NL
    Click cart icon
    Get products count from cart
    Run keyword if    ${cart_products_count} > 1    Remove all products from basket
    Show all products
    Choose buyable product with Colli unit
    Set global variable    ${pattern_unit}    Colli
    Verify expected unit highlighted
    Get palletisation info PDP
    Add to cart
    Click cart icon
    Set global variable    ${quantity_value}    1
    Set global variable    ${quantity_in_cases}     ${quantity_value}
    Verify expected unit highlighted
    Set how many palet required
    Calculate missing palets NL
    Verify error message for palet minimum quantity
    Verify process to checkout button disabled
    ${quantity_value}    BuiltIn.Evaluate    ${cases_per_layer}-1
    Set global variable    ${quantity_value}
    Set global variable    ${quantity_in_cases}     ${quantity_value}
    Insert quantity value
    Save cart changes
    Verify expected unit highlighted
    Calculate missing palets NL
    Verify error message for palet minimum quantity
    Verify process to checkout button disabled
    Set global variable    ${quantity_value}    ${cases_per_layer}
    Insert quantity value
    Save cart changes
    Set global variable    ${pattern_unit}    Laag
    Verify expected unit highlighted
    Verify error message for palet minimum quantity
    Verify process to checkout button disabled
    ${quantity_value}    BuiltIn.Evaluate    ${layer_per_palet}-1
    Calculate quantity in cases
    Set global variable    ${quantity_value}
    Calculate quantity in cases
    Insert quantity value
    Save cart changes
    Verify expected unit highlighted
    Calculate missing palets NL
    Verify error message for palet minimum quantity
    Verify process to checkout button disabled
    Set global variable    ${quantity_value}    ${layer_per_palet}
    Calculate quantity in cases
    Insert quantity value
    Save cart changes
    Set global variable    ${pattern_unit}    Pallet
    Verify expected unit highlighted
    Calculate missing palets NL
    Verify error message for palet minimum quantity
    Verify process to checkout button disabled
    Set global variable    ${quantity_value}    ${palet_required}
    Calculate quantity in cases
    Insert quantity value
    Save cart changes
    Verify expected unit highlighted
    Verify process to checkout button enabled
    Process to checkout
    Get products count from checkout
    Remember items quantity
    Collections.List should contain value    ${checkout_items_quantity}    ${quantity_value} pallet(s)

Validate palletisation info in cart
    [tags]    GB
    Get palletisation info cart
    BuiltIn.Should be equal as strings    ${palletisation_info_pdp}    ${palletisation_info_cart}
    Remove all products from basket
    #Close browser

#Validate cart quantity that allows to checkout

#Increase product quantity from cart
    #[tags]    BELUX    NL    FRANCE    GB
    #Remember item quantity
    #Increase item quantity
    #Increase item quantity
    #Remember item quantity
    #Should be equal as numbers    ${item_quantity}    ${item_quantity_before}+2
