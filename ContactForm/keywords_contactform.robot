*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Submit contact form
    Selenium Library.Click element    xpath=//button[@data-testid='ccep-submit-help-contact']

Verify form successfully submitted
    Sleep    10s
    Selenium Library.Wait until element is visible    xpath=//h2[@class='text-center thankyou__title']

Validate error messages visible for empty contact form
    Selenium Library.Element should be visible    xpath=//div[@class='ecp-dropdown form-group form-required']/div[@class='validation-feedback text-left']
    Selenium Library.Element should be visible    xpath=//div[@class='input input-textarea form-group text-right form-required']/div/div/div[@class='validation-feedback text-left']

Validate error messages not visible for empty contact form
    Selenium Library.Element should not be visible    xpath=//div[@class='ecp-dropdown form-group form-required']/div[@class='validation-feedback text-left']
    Selenium Library.Element should not be visible    xpath=//div[@class='input input-textarea form-group text-right form-required']/div/div/div[@class='validation-feedback text-left']

Select category options
    Count contact form categories
    Choose random category from contact form

Count contact form categories
    ${categories_count}    Selenium Library.Get Element Count    xpath=//ul[@class='dropdown-menu show']/li
    Set global variable    ${categories_count}

Choose random category from contact form
    Unfold categories dropdown
    Count contact form categories
    ${random_int}    BuiltIn.Evaluate    random.randint(1, ${categories_count})    modules=random
    Set global variable    ${random_int}
    Selenium Library.Click element   css=.show > li:nth-child(${random_int}) > .dropdown-item

Unfold categories dropdown
    Selenium Library.Click element    xpath=//form/div/div/div/div/button

Remember chosen category option
    ${category_option}    Selenium Library.Get Text    xpath=//div[@class='ecp-dropdown form-group form-required active']
    Set global variable    ${category_option}

Insert description
    Selenium Library.Input Text    id=ecp-input-contact-form-request-description    test description ${random_int}
    Set global variable    ${contact_decription}    test description ${random_int}

Select reason
    ${status}    Run keyword and return status    Selenium Library.Element should be visible    xpath=//div[@data-testid='ccep-dropdown-help-contact-reason']
    Run keyword if     '${status}' == 'True'
    ...    Run keywords    Choose random reason from contact form
    ...    AND    Remember chosen reason option
    Run keyword if     '${status}' == 'False'    Set none reason

Choose random reason from contact form
    Unfold reason options
    Count contact form reasons
    ${random_int}    BuiltIn.Evaluate    random.randint(1, ${reasons_count})    modules=random
    Set global variable    ${random_int}
    Selenium Library.Click element    css=.show > li:nth-child(${reasons_count}) > .dropdown-item

Unfold reason options
    Selenium Library.Click element    xpath=//div[2]/div/div/button

Count contact form reasons
    ${reasons_count}    Selenium Library.Get Element Count    xpath=//ul[@class='dropdown-menu show']/li
    Set global variable    ${reasons_count}

Remember chosen reason option
    ${reason_option}    Selenium Library.Get Text    xpath=//div[contains(@data-testid, 'ccep-dropdown-help-contact-reason')]
    Set global variable    ${reason_option}

Set none reason
    Set global variable    ${reason_option}    none
