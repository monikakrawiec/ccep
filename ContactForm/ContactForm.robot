*** Settings ***
Resource  keywords_contactform.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Validate contact form mandatory fields
    [tags]    BELUX    NL    FRANCE    GB
    Choose Help&Contact from top menu
    Validate error messages not visible for empty contact form
    Submit contact form
    Validate error messages visible for empty contact form
    Choose random category from contact form
    Remember chosen category option
    Select reason
    Insert description
    Submit contact form
    Verify form successfully submitted

Validate case in SalesForce
    [tags]    BELUX    NL    FRANCE    GB
    Go to Salesforce
    Log in to SalesForce
    Open SalesForce classic
    Show all tabs
    Go to cases
    Select all cases
    #Select all ECP cases
    #Show latest cases on top
    #Choose first case from list
    Run keyword if    '${usr}' == 'GB'    Map form with salesforce category GB
    Run keyword if    '${usr}' == 'BELUX'    Map form with salesforce category BELUX
    Run keyword if    '${usr}' == 'NL'    Map form with salesforce category NL
