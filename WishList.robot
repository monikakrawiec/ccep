*** Settings ***
Library  SeleniumLibrary    30
Library  String
Resource  keywords.robot


**Test Cases**

Prerequsites
    [tags]    orders
    #Open Browser    ${site_url}    ${browser}  remote_url=${RemoteUrl}  desired_capabilities=${DC} 
    Open CCEP
    Go to login page
    Log in to ECPT
    Verify user logged in
    Go to cart
    Get products count from cart
    Run keyword if    ${cart_products_count} > 1    Remove all products from basket

Add to new wishlist from PLP
    Show all products
    Remember random buyable product from PLP
    Click product star on PLP
    Create new wishlist
    Insert wishlist name 
    Create wishlist
    Verify wishlist saved
    Add product to wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Verify product saved in wishlist

Add to existing wishlist from PLP
    Set Global Variable    ${product_name_1}    ${product_name}  
    Show all products
    Remember random buyable product from PLP
    Set Global Variable    ${product_name_2}    ${product_name} 
    Click product star on PLP   
    Add to existing wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    @{product_names}    Create List    ${product_name_1}    ${product_name_2}
    :FOR    ${name}    IN    @{product_names}
    \    Set Test Variable    ${product_name}    ${name}    
    \    Verify product saved in wishlist

Add to existing wishlist from PDP
    Show all products
    Remember random buyable product from PLP
    Go to PDP of remembered product
    Click product star on PDP
    Add to existing wishlist PDP
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Verify product saved in wishlist

Add to cart from wishlist
    Remember wishlist products  
    Add all products to cart from wishlist
    Remember cart products
    Verify wishlist products in cart

Delete wishlist from MyAccount
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Delete wishlist
    Verify wishlist deleted

Add to wishlist from MyOrders
    [tags]    orders
    Choose MyOrders from top menu
    Remember products count from order
    Click wishlist star on MyOrders page
    Create new wishlist
    Insert wishlist name
    Create wishlist
    Verify wishlist saved
    Add product to wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Remember products count from wishlist
    Verify order products saved in wishlist

Update wishlist name
    Edit wishlist
    Update wishlist name
    Save wishlist name
    Verify wishlist name updated

Delete wishlist from MyAccount 2
    Delete wishlist
    Verify wishlist deleted
    Close Browser
     