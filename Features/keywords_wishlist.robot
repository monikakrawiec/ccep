*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Click product star on PLP
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random_int}]/div/div[2]/div/a/i

Click product star on PDP
    SeleniumLibrary.Click Element    css=.star > span

Create new wishlist
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .text-left > .content .btn
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'input-field__wrapper')]/input

Insert wishlist name
    SeleniumLibrary.Input Text    ecp-input-wishlist-name    wishlist${random_int}
    Set Global Variable    ${wishlist_name}    wishlist${random_int}    #variable will be used in 'Add to existing wishlist' keyword

Create wishlist
    SeleniumLibrary.Click Element    css=.btn-block:nth-child(2)
    SeleniumLibrary.Wait Until Element Contains    css=div:nth-child(1) > .text-left li:nth-child(1) > a    wishlist${random_int}

Verify wishlist saved
    SeleniumLibrary.Element Text Should Be    css=div:nth-child(1) > .text-left li:nth-child(1) > a     ${wishlist_name}

Add product to wishlist
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .text-left li:nth-child(1) .fa
    SeleniumLibrary.Wait Until Element Contains    css=div:nth-child(1) > .text-left li:nth-child(1) > a    ${wishlist_name}
    SeleniumLibrary.Wait Until Element Is Visible    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star

Close wishlist poopup
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .text-left > .footer > .btn
    Selenium Library.Wait until element is not visible    xpath=//div[@class='popover-body']

Choose wishlist from dashboard
    #SeleniumLibrary.Click Element    css=.list-block:nth-child(1)
    SeleniumLibrary.Click Element    xpath=//div[contains(@data-testid, 'ccep-div-order-widget-wishlist')]
    SeleniumLibrary.Wait Until Element Contains    xpath=//div[@id='tab-mylists']/div/div[2]/div/div/div/div/div[2]/h4    ${wishlist_name}

Verify product saved in wishlist
    Sleep    5s
    SeleniumLibrary.Element Text Should Be    css=.font-weight-bold    ${product_name}
    #SeleniumLibrary.Page Should Contain    ${product_name}

Delete wishlist
    SeleniumLibrary.Click Element    xpath=//a[contains(@class, 'fa fa-trash-o')]
    SeleniumLibrary.Click Element    css=.pb-2 div
    Sleep    3s

Verify wishlist deleted
    Sleep    5s
    SeleniumLibrary.Element Should Not Be Visible    wishlist${random_int}

Verify product added to wishlist
    SeleniumLibrary.Element Should Be Visible    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star

Remove product from wishlist
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star
    SeleniumLibrary.Wait Until Page Contains Element    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star-o

Verify product removed from wishlist
    SeleniumLibrary.Element Should Not Be Visible    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star
    SeleniumLibrary.Element Should Be Visible    css=div:nth-child(1) > .text-left li:nth-child(1) .fa.fa-star-o

Remember wishlist products
    Get products count from wishlist
    @{wishlist_products}    Create List
    :FOR    ${i}    IN RANGE    1    ${wishlist_products_count}+1
    \    ${name}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'card-body mt-3 relative')]/div/div[contains(@class, 'item')][${I}]/div/div[3]/div
    \    Collections.Append To List    ${wishlist_products}    ${name}
    Set Global Variable    @{wishlist_products}

Get products count from wishlist
    ${wishlist_products_count}    SeleniumLibrary.Get Element Count    xpath=//div[contains(@class, 'card-body mt-3 relative')]/div/div
    Set Global Variable    ${wishlist_products_count}

Add all products to cart from wishlist
    SeleniumLibrary.Click Element    css=button.btn.mt-2.add-to-cart.btn-primary
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//form[contains(@class, 'checkout')]

Verify wishlist products in cart
    Lists Should Be Equal    ${wishlist_products}    ${cart_products}

Edit wishlist
    SeleniumLibrary.Click Element    css=.fa-pencil

Update wishlist name
    SeleniumLibrary.Input Text    id=ecp-input-wishlist-label    ${wishlist_name} updated

Save wishlist name
    SeleniumLibrary.Click Element    css=.btn-save > span

Verify wishlist name updated
    SeleniumLibrary.Wait Until Element Contains    css=h4.d-inline-block    ${wishlist_name} updated
    SeleniumLibrary.Element Text Should Be    css=h4.d-inline-block   ${wishlist_name} updated

Delete product from wishlist
    Selenium Library.Click element    xpath=//i[contains(@data-testid, 'ccep-button-product-list-item-remove')]
    Confirm deletion from wishlist

Confirm deletion from wishlist
    #Selenium Library.Click element    xpath=//button[contains(@data-testid, 'ccep-button-product-list-item-remove-okay')]/span
    Selenium Library.Click element    css=div:nth-child(1) > .pb-2 span
    Selenium Library.Wait until element is not visible    xpath=//div[@class='popover-body']
