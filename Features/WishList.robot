*** Settings ***
Library  SeleniumLibrary    30
Library  String
Resource  keywords_wishlist.robot


**Test Cases**

Prerequsites
    [tags]    BELUX    NL    FRANCE    GB
    #Open Browser    ${site_url}    ${browser}  remote_url=${RemoteUrl}  desired_capabilities=${DC}
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in
    Go to cart
    Get products count from cart
    Run keyword if    ${cart_products_count} > 1    Remove all products from basket

Add to new wishlist from PLP
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Get products quantity from PLP
    Remember random buyable product from PLP
    Click product star on PLP
    Create new wishlist
    Insert wishlist name
    Create wishlist
    Verify wishlist saved
    Add product to wishlist
    Verify product added to wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Verify product saved in wishlist

Remove from wishlist from PLP
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Click product star on PLP
    Remove product from wishlist
    Verify product removed from wishlist
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Get products count from wishlist
    BuiltIn.Should be equal as numbers    ${wishlist_products_count}    0

Add to existing wishlist from PLP
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Remember random buyable product from PLP
    Click product star on PLP
    Add to existing wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Verify product saved in wishlist

Add to existing wishlist from PDP
    [tags]    BELUX    NL    FRANCE    GB
    Show all products
    Remember random buyable product from PLP
    Go to PDP of remembered product
    Click product star on PDP
    Add to existing wishlist PDP
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Verify product saved in wishlist

Add to cart from wishlist
    [tags]    BELUX    NL    FRANCE    GB
    Remember wishlist products
    Add all products to cart from wishlist
    Remember basket items
    Verify wishlist products in cart

Remove product from wishlist
    [tags]    BELUX    NL    FRANCE    GB
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Get products count from wishlist
    Set global variable    ${wishlist_products_count_before_deletion}    ${wishlist_products_count}
    Delete product from wishlist
    Get products count from wishlist
    ${wishlist_products_count_before_deletion}    Evaluate    ${wishlist_products_count_before_deletion}-1
    BuiltIn.Should be equal as numbers    ${wishlist_products_count_before_deletion}    ${wishlist_products_count}

Delete wishlist from MyAccount
    [tags]    BELUX    NL    FRANCE    GB
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Delete wishlist
    Verify wishlist deleted

Add to wishlist from MyOrders
    [tags]    BELUX    NL    FRANCE    GB
    Choose MyOrders from top menu
    Remember products count from order
    Click wishlist star on MyOrders page
    Create new wishlist
    Insert wishlist name
    Create wishlist
    Verify wishlist saved
    Add product to wishlist
    Close wishlist poopup
    Choose Dashboard from top menu
    Choose wishlist from dashboard
    Remember products count from wishlist
    Verify order products saved in wishlist

Update wishlist name
    [tags]    BELUX    NL    FRANCE    GB
    Edit wishlist
    Update wishlist name
    Save wishlist name
    Verify wishlist name updated

Delete wishlist from MyAccount 2
    [tags]    BELUX    NL    FRANCE    GB
    Delete wishlist
    Verify wishlist deleted
    Close Browser
