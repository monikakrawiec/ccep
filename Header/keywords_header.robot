*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Select locale from top menu
    Selenium Library.Click element    xpath=//a[@class='header-nav-link header-locale-dropdown__btn-link dropdown-toggle']
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Click link    link=English (GB)
    Run keyword if    '${usr}' == 'IBERIA'    Selenium Library.Click link    link=Español (ES)
    Run keyword if    '${usr}' == 'BELUX'    Selenium Library.Click link    link=Français (BE)
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Click link    link=Nederlands (NL)
    Run keyword if    '${usr}' == 'FRANCE'    Selenium Library.Click link    link=Français (FR)

Verify cookie consent visible
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'cookie-consent')]

Click cookie consent link
    SeleniumLibrary.Click Element       xpath=//a[contains(@href, '/legal/cookie')]
    Sleep    3s

Verify cookie consent link redirection
    SeleniumLibrary.Select Window    url=${site_url}/legal/cookie
    Location Should Be    ${site_url}/legal/cookie
    SeleniumLibrary.Close Window
    SeleniumLibrary.Select Window
    Sleep    2s

Confirm cookie consent terms
    SeleniumLibrary.Click Element    xpath=//a[contains(@data-testid, 'ccep-a-cookie-consent-btn')]

Verify cookie consent not shown
    SeleniumLibrary.Element Should Not Be Visible    xpath=//div[contains(@class, 'cookie-consent')]

Choose Home from side menu
    Click menu icon
    Select Home link from side menu

Choose Brands from side menu
    Click menu icon
    Select Brands link from side menu

Choose News from side menu
    Click menu icon
    Select News link from side menu

Choose Help&Contact from side menu
    Click menu icon
    Select Help&Contact link from side menu

Click menu icon
    SeleniumLibrary.Click Element    css=.header-nav__toggler-wrapper .header-nav-toggler__icon

Select Home link from side menu
    SeleniumLibrary.Click Element    xpath=//span[contains(.,'HOME')]

Select Brands link from side menu
    #SeleniumLibrary.Click Element    xpath=//span[contains(.,'Brands')]
    SeleniumLibrary.Click Element    xpath=//a[@href='/brands']/span
    #SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'brand-overview view')]
    Sleep    5s

Select News link from side menu
    SeleniumLibrary.Click Element    xpath=//span[contains(.,'News')]
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//section[contains(@class, 'news-overview view')]

Select Help&Contact link from side menu
    SeleniumLibrary.Click Element    xpath=//span[contains(.,'Help & Contact')]
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'help view container')]

Choose Dashboard from top menu
    SeleniumLibrary.Click Element    css=.header-nav-item:nth-child(1) span
    Selenium Library.Wait until element is visible    xpath=//div[@class='dashboard view container']
    Selenium Library.Wait until element is visible    xpath=//span[@class='dashboard__title']
    Sleep    5s

Choose Brands from top menu
    SeleniumLibrary.Click Element    xpath=//li[@class='header-nav-item header-navbar__item'][4]/a/span

Choose News from top menu
    Run keyword unless    '${usr}' == 'BELUX'    SeleniumLibrary.Click Element    xpath=//li[@class='header-nav-item header-navbar__item'][5]/a/span
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Click Element    xpath=//li[@class='header-nav-item header-navbar__item'][5]/div/button/a

Choose News from top menu dropdown
    Selenium Library.Click element    xpath=(//a[contains(@href, '/news')])[2]

Choose Topics from top menu
    Selenium Library.Click element    xpath=(//a[contains(@href, '/topics')])[2]

Choose MyAccount from top menu
    Selenium Library.Click element    xpath=//li[@class='header-nav-item header-navbar__item'][2]/div/button/a
    Selenium Library.Wait until element is visible    xpath=//ul[@class='dropdown-menu header-dropdown__list show']
    Selenium Library.Wait until element is visible    xpath=(//a[contains(@href, '/myaccount/profile')])[2]
    Sleep    2s

Choose MyProfile from top menu
    Selenium Library.Click element    xpath=(//a[contains(@href, '/myaccount/profile')])[2]
    Sleep    3s

Choose MyInvoices from top menu
    Selenium Library.Click element    xpath=(//a[contains(@href, '/myaccount/invoices')])[2]

Choose MyRequests from top menu
    Selenium Library.Click element    xpath=(//a[contains(@href, '/myaccount/requests')])[2]
    Selenium Library.Wait until element is visible    xpath=//div[@class='requests-overview maxi view container']

Choose ShopProducts from top menu
    Selenium Library.Click element    xpath=//li[@class='header-nav-item header-navbar__item'][3]/div/button/a
    Sleep    2s

Choose MyLists from top menu
    Selenium Library.Click element    xpath=(//a[contains(@href, '/shop/mylists')])[2]

Choose AllProducts from top menu
    Selenium Library.Click Element    xpath=(//a[contains(@href, '/shop/')])[6]

Choose MyOrders from top menu
    Selenium Library.Click Element    xpath=(//a[contains(@href, '/shop/myorders')])[2]
    SeleniumLibrary.Wait Until Page Contains Element    id=tab-myorders
    Selenium Library.Wait until page contains element    xpath=//div[contains(@class, 'orders')]
    Run keyword if    '${usr}' == 'NL'    Selenium Library.Wait until page contains element    xpath=//div[contains(@class, 'product-filter')]
    Sleep    5s

CC logo visible in header
    SeleniumLibrary.Element Should Be Visible    xpath=//a[@data-testid='ccep-a-header-brand-2']

Click CC logo in header
    SeleniumLibrary.Click Element    css=.header-brand:nth-child(4)

Verify home page redirection for not logged in user
    SeleniumLibrary.Location Should Be    ${site_url}/

Verify home page redirection for logged in user
    SeleniumLibrary.Location Should Be    ${site_url}/home

Verify top menu elements visibility
    Run keyword if    '${usr}' == 'IBERIA'    Verify top menu elements Iberia
    Run keyword if    '${usr}' == 'BELUX'    Verify top menu elements Belux
    Run keyword if    '${usr}' == 'NL'    Verify top menu elements Netherlands
    Run keyword if    '${usr}' == 'FRANCE'    Verify top menu elements France
    Run keyword if    '${usr}' == 'GB'    Verify top menu elements GB

Verify top menu elements Iberia
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    PANEL DE CONTROL
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    mi cuenta
    Choose MyAccount from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Mi perfil
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mis facturas
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mis solicitudes
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(4) > .dropdown-item    Mis informes 347
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/a/span    MARCAS
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    Noticias
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/a/span    Ayuda y Contacto

Verify top menu elements Belux
    Belux set locale to FR
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    MON RÉSUMÉ
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    MON COMPTE
    Choose MyAccount from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Mon Profil
    #SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mes Factures
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mes Demandes
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/div/button/a    COMMANDER
    Choose ShopProducts from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Tous les produits
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mes listes
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mes Commandes
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    MARQUES
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/div/button/a    NOUVELLES
    Choose News from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Nouvelles
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Votre distributeur Coca-Cola
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[6]/a/span    AIDE ET CONTACT
    Belux set locale to NL
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    HOME
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    MIJN ACCOUNT
    Choose MyAccount from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Mijn Profiel
    #SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mijn Facturen
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mijn Aanvragen
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/div/button/a    BESTELLEN
    Choose ShopProducts from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Alle producten
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mijn lijsten
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mijn bestellingen
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    MERKEN
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/div/button/a    NIEUWS
    Choose News from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Nieuws
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Coca-Cola Vending
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[6]/a/span    HULP & CONTACT

Belux set locale to NL
    ${current_locale}    Get Text    xpath=(//a[@id='localeDropdown'])[2]
    Run keyword if    '${current_locale}' == 'FR'    SeleniumLibrary.Click Link    link=FR
    Run keyword if    '${current_locale}' == 'FR'    SeleniumLibrary.Click Link    link=Nederlands (BE)
    Selenium Library.Wait until element contains    xpath=(//a[@id='localeDropdown'])[2]    NL

Belux set locale to FR
    ${current_locale}    Get Text    xpath=(//a[@id='localeDropdown'])[2]
    Run keyword if    '${current_locale}' == 'NL'    SeleniumLibrary.Click Link    link=NL
    Run keyword if    '${current_locale}' == 'NL'    SeleniumLibrary.Click Link    link=Français (BE)

Verify top menu elements Netherlands
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    Dashboard
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    Mijn Account
    Choose MyAccount from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Mijn Profiel
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mijn Facturen
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Mijn Verzoeken
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/div/button/a    Producten Bestellen
    Choose ShopProducts from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    Mijn Lijsten
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    Mijn Bestellingen
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    Alle Producten
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    Merken
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/a/span    Nieuws
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[6]/a/span    Service & Contact

Verify top menu elements France
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    Dashboard
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    My account
    Choose MyAccount from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    My Profile
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    My Invoices
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    My Requests
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/div/button/a    Shop Products
    Choose ShopProducts from top menu
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    My Lists
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    My Orders
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    Brands
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/a/span    News
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[6]/a/span    Help & Contact

Verify top menu elements clickable and highlighted
    Verify Dashboard clickable
    Verify MyProfile clickable
    Verify MyInvoices clickable
    Verify MyRequests clickable
    Verify MyLists clickable
    Verify MyOrders clickable
    Verify Brands clickable
    Verify News clickable
    Run keyword if    '${usr}' == 'BELUX'    Verify Topics clickable
    Verify Help&Contact clickable

Verify Dashboard clickable
    Choose Dashboard from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/home
    Verify Dashboard highlighted

Verify MyProfile clickable
    Choose MyAccount from top menu
    Choose MyProfile from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/myaccount/profile
    Verify MyAccount highlighted

Verify MyInvoices clickable
    Choose MyAccount from top menu
    Choose MyInvoices from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/myaccount/invoices
    Verify MyAccount highlighted

Verify MyRequests clickable
    Choose MyAccount from top menu
    Choose MyRequests from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/myaccount/requests
    Verify MyAccount highlighted

Verify MyLists clickable
    Choose ShopProducts from top menu
    Choose MyLists from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/shop/mylists
    Verify ShopProducts highlighted

Verify MyOrders clickable
    Choose ShopProducts from top menu
    Choose MyOrders from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/shop/myorders
    Verify ShopProducts highlighted

Verify Brands clickable
    Choose Brands from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/brands
    Verify Brands highlighted

Verify News clickable
    Choose News from top menu
    Run keyword if    '${usr}' == 'BELUX'    Choose News from top menu dropdown
    SeleniumLibrary.Location Should Be    ${site_url}/news
    Verify News highlighted

Verify Topics clickable
    Choose News from top menu
    Choose topics from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/topics
    Verify News highlighted

Verify Help&Contact clickable
    Choose Help&Contact from top menu
    SeleniumLibrary.Location Should Be    ${site_url}/help
    Verify Help&Contact highlighted

Verify top menu elements GB
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li/a/span    Dashboard
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[2]/div/button/a    My account
    SeleniumLibrary.Click Link    link=My account
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    My Profile
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    My Invoices
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    My Requests
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[3]/div/button/a    Shop Products
    SeleniumLibrary.Click Link    link=Shop Products
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(1) > .dropdown-item    My Lists
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(2) > .dropdown-item    My Orders
    SeleniumLibrary.Element Text Should Be    css=.show > li:nth-child(3) > .dropdown-item    All products
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[4]/a/span    Brands
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[5]/a/span    News
    SeleniumLibrary.Element Text Should Be    xpath=//div[@id='app']/header/div/nav/ul/li[6]/a/span    Help & Contact

Verify Dashboard highlighted
    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[1]/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify MyAccount highlighted
    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[2]/div/button/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify ShopProducts highlighted
    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[3]/div/button/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify Brands highlighted
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[3]/a[contains(@class, 'header-nav-link header-nav-item__link show')]
    Run keyword unless    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[4]/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify News highlighted
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[4]/a[contains(@class, 'header-nav-link header-nav-item__link show')]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[5]/div/button/a[contains(@class, 'header-nav-link header-nav-item__link show')]
    Run keyword if    '${usr}' == 'NL' or '${usr}' == 'FRANCE' or '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[5]/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify Help&contact highlighted
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[5]/a[contains(@class, 'header-nav-link header-nav-item__link show')]
    Run keyword unless    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/header/div/nav/ul[1]/li[6]/a[contains(@class, 'header-nav-link header-nav-item__link show')]

Verify side menu elements visibility
    SeleniumLibrary.Element Text Should Be    css=.header-navburger__item:nth-child(1) span    HOME
    SeleniumLibrary.Element Text Should Be    css=.header-navburger__item:nth-child(2) span    Brands
    SeleniumLibrary.Element Text Should Be    css=.header-navburger__item:nth-child(3) span    News
    SeleniumLibrary.Element Text Should Be    css=.header-navburger__item:nth-child(4) span    Help & Contact

Verify side menu elements clickable
    Verify Home clickable in side menu
    Verify Brands clickable in side menu
    Verify News clickable in side menu
    Verify Help&Contact clickable side menu

Verify Home clickable in side menu
    Choose Home from side menu
    SeleniumLibrary.Location Should Be    ${site_url}/

Verify Brands clickable in side menu
    Choose Brands from side menu
    SeleniumLibrary.Location Should Be    ${site_url}/brands

Verify News clickable in side menu
    Choose News from side menu
    SeleniumLibrary.Location Should Be    ${site_url}/news

Verify Help&Contact clickable side menu
    Choose Help&Contact from side menu
    SeleniumLibrary.Location Should Be    ${site_url}/help

Change locale
    Open locale dropdown
    ${locale_count}    SeleniumLibrary.Get Element Count    xpath=//ul[contains(@class, 'dropdown-menu header-dropdown__list header-dropdown__list--right header-locale-dropdown__list dropdown-menu-right show')]/li
    ${random int}    Evaluate    random.randint(1, ${locale_count})    modules=random
    SeleniumLibrary.Click Element    xpath=//ul[contains(@class, 'dropdown-menu header-dropdown__list header-dropdown__list--right header-locale-dropdown__list dropdown-menu-right show')]/li[${random int}]/a
    Sleep    2s

Check current locale
    ${current_locale}    SeleniumLibrary.Get Text    id=localeDropdown
    Set global variable    ${current_locale}

Get locale list from dropdown
    Open locale dropdown
    @{locale_list}    Create List
    ${locale_count}    SeleniumLibrary.Get Element Count    xpath=//ul[contains(@class, 'dropdown-menu header-dropdown__list header-dropdown__list--right header-locale-dropdown__list dropdown-menu-right show')]/li
    :FOR    ${i}    IN RANGE    1    ${locale_count}+1
    \    ${locale_name}    SeleniumLibrary.Get Text    xpath=//ul[contains(@class, 'dropdown-menu header-dropdown__list header-dropdown__list--right header-locale-dropdown__list dropdown-menu-right show')]/li[${i}]/a
    \    Append to list    ${locale_list}    ${locale_name}
    Set global variable    @{locale_list}

Open locale dropdown
    SeleniumLibrary.Click Element    xpath=(//a[@id='localeDropdown'])[2]

Choose Help&Contact from top menu
    Selenium Library.Click element    xpath=//li[@class='header-nav-item header-navbar__item'][6]/a/span
    Selenium Library.Wait until page contains element    id=contact
