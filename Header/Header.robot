*** Settings ***
Resource  keywords_header.robot

**Test Cases**

Prerequisites
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Open CCEP
    Select locale from top menu

Cookie consent validation
    #[tags]    IBERIA    BELUX    NL    FRANCE    GB
    Verify cookie consent visible
    Click cookie consent link
    Verify cookie consent link redirection
    Change locale
    Verify cookie consent visible
    Confirm cookie consent terms
    Verify cookie consent not shown
    Change locale
    Verify cookie consent not shown

CC logo visible on home page for not logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    CC logo visible in header

CC logo directs to home page for not logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Choose Brands from side menu
    Click CC logo in header
    Verify home page redirection for not logged in user

CC logo visible on home page for logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Go to login page
    Log in to CCEP
    Verify user logged in
    CC logo visible in header

CC logo directs to dashboard for logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Sleep    3s
    Choose Brands from top menu
    Click CC logo in header
    Verify home page redirection for logged in user

Validate top navigation for logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Verify top menu elements visibility
    Verify top menu elements clickable and highlighted
    Run keyword if    '${usr}' == 'BELUX'    Run keywords
    ...    Belux set locale to NL
    ...    AND    Verify top menu elements clickable and highlighted    # additional validation for BELUX NL language

Validate side navigation for not logged in user
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Delete cookies
    Click menu icon
    Verify side menu elements visibility
    Delete cookies
    Verify side menu elements clickable
    Close Browser
