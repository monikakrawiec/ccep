*** Settings ***
Library    SeleniumLibrary    20
Library    String
Library    Collections
Library    DateTime
Library    OperatingSystem
Library    RequestsLibrary
Library     CustomLibraries/ListOperations.py
Library     CustomLibraries/MathOperations.py
Library     CustomLibraries/Weekday.py
Resource    Variables.txt
Resource    Cart/keywords_cart.robot
Resource    CheckoutProcess/keywords_checkout.robot
Resource    Features/keywords_wishlist.robot
Resource    Footer/keywords_footer.robot
Resource    Header/keywords_header.robot
Resource    MyAccount/keywords_myaccount.robot
Resource    MyDashboard/keywords_mydashboard.robot
Resource    MyOrders/keywords_myorders.robot
Resource    ProductDetailPage/keywords_pdp.robot
Resource    ProductListingPage/keywords_plp.robot
Resource    Salesforce/keywords_salesforce.robot
Resource    UserAuthentication/keywords_login.robot
Resource    Test/keywords_test.robot


**Keywords**

Set environment
    Run keyword if    '${env}'=='int'    Set Global Variable   ${site_url}    https://int-serviceportal.cs109.force.com
    #Run keyword if    '${env}'=='ecpt'    Set Global Variable   ${site_url}    https://ecpt-serviceportal.cs108.force.com
    Run keyword if    '${env}'=='ecpt'    Set Global Variable   ${site_url}    https://ecpt2-serviceportal.cs85.force.com

Set user
    Run keyword if    '${usr}' == 'BELUX'
    ...    Run keywords
    ...    Run keyword if    '${env}' == 'int'    Set Belux int user
    ...    AND    Run keyword if    '${env}' == 'ecpt'    Set Belux ecpt user
    Run keyword if    '${usr}' == 'IBERIA'
    ...    Run keywords
    ...    Run keyword if    '${env}' == 'int'    Set Iberia int user
    ...    AND    Run keyword if    '${env}' == 'ecpt'    Set Iberia ecpt user
    Run keyword if    '${usr}' == 'NL'
    ...    Run keywords
    ...    Run keyword if    '${env}' == 'int'    Set Netherlands int user
    ...    AND    Run keyword if    '${env}' == 'ecpt'    Set Netherlands ecpt user
    Run keyword if    '${usr}' == 'FRANCE'
    ...    Run keywords
    ...    Run keyword if    '${env}' == 'int'    Set France int user
    ...    AND    Run keyword if    '${env}' == 'ecpt'    Set France ecpt user
    Run keyword if    '${usr}' == 'GB'
    ...    Run keywords
    ...    Run keyword if    '${env}' == 'int'    Set GB int user
    ...    AND    Run keyword if    '${env}' == 'ecpt'    Set GB ecpt user

Set Belux int user
    Set global variable    ${user}    prodtestccep+int_bepayer1@gmail.com

Set Belux ecpt user
    Set global variable     ${user}    suyo.automation+ecpt_belux@gmail.com

Set Iberia ecpt user
    Set global variable     ${user}    suyo.automation+ecpt_iberia@gmail.com

Set Iberia int user
    Set global variable     ${user}    prodtestccep+int_qa7@gmail.com

Set Netherlands ecpt user
    Set global variable     ${user}    suyo.automation+ecpt2_nl1@gmail.com

Set Netherlands int user
    Set global variable     ${user}    prodtestccep+int_nl98@gmail.com

Set France ecpt user
    Set global variable     ${user}    suyo.automation+ecpt_fr@gmail.com

Set France int user
    Set global variable     ${user}    prodtestccep+int_nl98@gmail.com

Set GB ecpt user
    Set global variable     ${user}    suyo.automation+ecpt2_gb@gmail.com

Set GB int user
    Set global variable     ${user}    prodtestccep+int_gb1@gmail.com

Open CCEP
    Set Environment Variable    webdriver.gecko.driver    /usr/local/bin/geckodriver
    Set environment
    Set user
    SeleniumLibrary.Open Browser    ${site_url}    ${browser}    ###remote_url=${RemoteUrl}    desired_capabilities=${DC}
    Sleep    10s
    #SeleniumLibrary.Maximize Browser Window
    SeleniumLibrary.Set Window Size    1920         1200
    Accept cookie
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'landing')]

Go to CCEP
    Selenium Library.Go to    ${site_url}
    Selenium Library.Wait until element is visible    xpath=//div[@class='dashboard view container']
    Selenium Library.Wait until element is visible    xpath=//i[@class='fa fa-calendar']

Click user icon
    SeleniumLibrary.Click Element    css=.header-dropdown-user__icon

Go to login page
    Click user icon
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Wait Until Page Contains Element    name=login-email
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Wait Until Page Contains Element    name=email

Click Coke logo
    SeleniumLibrary.Click Element    css=a.header-brand

Click wishlist star on MyOrders page
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'wishlist-icon wishlist-icon--custom wishlist-icon--medium')]/a/i

Remember products count from order
    ${products_number}    SeleniumLibrary.Get Text    css=.row:nth-child(1) > .col .row h5
    @{products_number}    String.Split String    ${products_number}    ${SPACE}
    Set Global Variable    ${order_product_number}    @{products_number}[0]

Remember products count from wishlist
    ${products_number}    SeleniumLibrary.Get Text    css=h5:nth-child(3)
    @{products_number}    String.Split String    ${products_number}    ${SPACE}
    Set Global Variable    ${wishlist_product_number}    @{products_number}[0]

Verify order products saved in wishlist
    Should Be Equal As Strings    ${order_product_number}    ${wishlist_product_number}

Add to existing wishlist
    #SeleniumLibrary.Click Element    xpath=(//a[contains(text(),'${wishlist_name}')])[29]
    SeleniumLibrary.Click Element    css=div:nth-child(1) > .text-left li:nth-child(1) > a

Add to existing wishlist PDP
    SeleniumLibrary.Click Element    css=li:nth-child(1) .float-right > .fa

Delete cookies
    SeleniumLibrary.Delete All Cookies
    SeleniumLibrary.Reload Page
    Sleep    5s
    Accept cookie

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})

Set locale to EN
    Selenium Library.Click element    id=localeDropdown
    Selenium Library.Click element    xpath=//a[@data-testid, 'ccep-select-header-locale-enGB']

Accept cookie
    Selenium Library.click element    css=.accept-cookies-button
    Sleep    3s
