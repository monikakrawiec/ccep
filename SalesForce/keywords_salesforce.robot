*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Go to Salesforce
    #Selenium Library.Go to    https://ccep--ecpt.cs108.my.salesforce.com/home/home.jsp
    Selenium Library.Go to    https://ccep--ecpt2.my.salesforce.com/home/home.jsp
    Selenium Library.Wait until element is visible    xpath=//img[@alt='Salesforce']

Log in to SalesForce
    Selenium Library.Input text    id=username    ${salesforce_user}
    Selenium Library.Input text    id=password    ${salesforce_password}
    Selenium Library.Click element    id=Login
    Sleep    5s

Check SalesForce interface
    ${status}    Run keyword and return status    Selenium Library.Element should be visible    xpath=//span[@class='uiImage']
    Set global variable      ${status}

Open SalesForce classic
    Check SalesForce interface
    Run keyword if    '${status}' == 'True'    Run keywords
    ...    Selenium Library.Click element     xpath=//span[@class='uiImage']
    ...    AND    Selenium Library.Click link    link=Switch to Salesforce Classic
    ...    AND    Selenium Library.Wait until element is visible    xpath=//img[@alt='Salesforce.com']
    ...    AND    Sleep    5s

Show all tabs
    Go to    https://ccep--ecpt2.my.salesforce.com/home/showAllTabs.jsp
    Sleep    3s

Go to ECP Members
    Selenium Library.Click element    xpath=//a[@href='/aDx/o']

Show all members
    Selenium Library.Click element    name=go

Search for member
    Selenium Library.Input text    id=phSearchInput    ${user}
    Selenium Library.Click element    id=searchButtonContainer
    Sleep    5s

Enter member details
    Selenium Library.Click element    xpath=//div[@id='ECP_Member__c_body']/table/tbody/tr[2]/th/a
    #Selenium Library.Click element    xpath=//div[6]/div/div[2]/div/div[2]/table/tbody/tr[2]/th/a
    Selenium Library.Wait until element contains    xpath=//h2[@class="mainTitle"]    ECP Member Detail

Remember user data
    Remember member first name
    Remember member last name
    Remember member email
    Remember member phone number
    #Remember member mobile number

Remember member first name
    ${sf_first_name}    Selenium Library.Get text    id=00N1i00000112Vz_ileinner
    Set global variable    ${sf_first_name}

Remember member last name
    ${sf_last_name}    Selenium Library.Get text    id=00N1i00000112W3_ileinner
    Set global variable    ${sf_last_name}

Remember member email
    ${sf_email}    Selenium Library.Get text    xpath=//*[@id='00N1i00000112Vy_ileinner']/a
    Set global variable    ${sf_email}

Remember member phone number
    ${sf_phone_number}    Selenium Library.Get text    xpath=//td[contains(@class, 'PhoneNumberElement')]
    Set global variable    ${sf_phone_number}

#Remember member mobile number

Search for order
    Selenium Library.Input text    id=phSearchInput    ${order_number}

Submit order search
    :FOR    ${i}    IN RANGE    1    10
    \    Search for order
    \    Selenium Library.Click element    id=searchButtonContainer
    \    ${status}    Run keyword and return status    Selenium Library.Wait until element contains    xpath=//*[@id="ccrz__E_Order__c_body"]/table/tbody/tr[2]/th/a    ${order_number}
    \    Exit for loop if    '${status}' == 'True'

Enter details of order
    Selenium Library.Click link    xpath=//a[contains(text(),'${order_number}')]
    Selenium Library.Wait until element contains    xpath=//h2[@class='mainTitle']    CC Order Detail

Remember salesforce items
    #Get products count from salesforce
    @{salesforce_products}    Create List
    :FOR    ${i}    IN RANGE    2    ${checkout_products_count}+1    # counts from 2 because first div is a container title
    \    ${name}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'listRelatedObject customnotabBlock')]/div/div/table/tbody/tr[${i}]/td[2]/a
    \    Collections.Append To List    ${salesforce_products}    ${name}
    Set Global Variable    @{salesforce_products}

Get products count from salesforce
    ${salesforce_products_count}    SeleniumLibrary.Get Element Count    //*[@id="a4r1l00000006yB_00NU0000003kUDQ_body"]/table/tbody/tr
    Set Global Variable    ${salesforce_products_count}

Remember salesforce items quantity
    @{salesforce_items_quantity}    Create List
    :FOR    ${i}    IN RANGE    2    ${checkout_products_count}+1    # counts from 2 because first div is a container title
    \    ${qty}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'listRelatedObject customnotabBlock')]/div/div/table/tbody/tr[${i}]/td[3]
    \    Collections.Append To List    ${salesforce_items_quantity}    ${qty}
    Set Global Variable    @{salesforce_items_quantity}

Compare checkout items with salesforce order
    Collections.Lists should be equal    ${checkout_products}    ${salesforce_products}

Verify product quantities saved correctly
    :FOR    ${i}    IN RANGE    0    ${checkout_products_count}-1    #header also included
    \    BuiltIn.Should Contain    @{checkout_items_quantity}[${i}]    @{salesforce_items_quantity}[${i}]

Go to cases
    Selenium Library.Click link    link=Cases
    #Selenium Library.Click element    xpath=//*a[@class='listRelatedObject caseBlock title']/img

Select all ECP cases
    Selenium Library.Click element    id=fcf
    Selenium Library.Select From List By Label    id=fcf    All Cases_ECP
    Selenium Library.Click Element    name=go

Select all cases
    Selenium Library.Click element    id=fcf
    Selenium Library.Select From List By Label    id=fcf    All Cases
    Selenium Library.Click Element    name=go

Show latest cases on top
    #Selenium Library.Click element    xpath=//div[@title='Date/Time Opened']
    Selenium Library.Click Element    xpath=//div[contains(@class, 'CASES_CASE_NUMBER')]
    Sleep    3s
    Selenium Library.Click Element    xpath=//div[contains(@class, 'CASES_CASE_NUMBER')]
    Selenium Library.Wait until element is visible    xpath=//td[contains(@class, 'td-CASES_CASE_NUMBER DESC')]

Choose first case from list
    #Selenium Library.Click element    xpath=//tr[1]/td[contains(@class, 'td-CASES_CASE_NUMBER')]/div/a
    Selenium Library.Click element    xpath=//tbody/tr/td[4]/div/a
    Selenium library.Wait Until Element Contains    xpath=//h2[@class='mainTitle']    Case Detail

Map form with salesforce category GB
    Get SF case category
    Get SF case subcatgory
    Run keyword if    '${category_option}' == 'Question about orders'    Should be equal as strings    ${sf_category}    Order
    Run keyword if    '${reason_option}' == 'Level of Service'    Should be equal as strings    ${sf_subcategory}    Level Of Service
    Run keyword if    '${reason_option}' == 'Ordering'    Should be equal as strings    ${sf_subcategory}    Ordering
    Run keyword if    '${reason_option}' == 'Uplift/Credit'    Should be equal as strings    ${sf_subcategory}    Uplift/Credit
    Run keyword if    '${reason_option}' == 'Delivery'    Should be equal as strings    ${sf_subcategory}    Delivery
    Run keyword if    '${reason_option}' == 'Stock'    Should be equal as strings    ${sf_subcategory}  		Stock
    Run keyword if    '${reason_option}' == 'Stock'    Should be equal as strings    ${sf_subcategory}  		Stock
    Run keyword if    '${category_option}' == 'Contact question'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Contact
    ...    AND    Should be equal as strings    ${sf_subcategory}    none
    Run keyword if    '${category_option}' == 'Question about account'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Account
    ...    AND    Should be equal as strings    ${sf_subcategory}    none
    Run keyword if    '${category_option}' == 'Other questions'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Other
    ...    AND    Should be equal as strings    ${sf_subcategory}    none
    Run keyword if    '${category_option}' == 'Question about the portal'    Should be equal as strings    ${sf_category}    Customer Portal
    Run keyword if    '${reason_option}' == 'Customer support'    Should be equal as strings    ${sf_subcategory}    Customer Support
    Run keyword if    '${reason_option}' == 'Technical support'    Should be equal as strings    ${sf_subcategory}    Technical Support

Map form with salesforce category BELUX
    Get SF case category
    Get SF case subcatgory
    Run keyword if    '${category_option}' == 'À propos de my.ccep.com'    Should be equal as strings    ${sf_category}    Customer Portal
    Run keyword if    '${reason_option}' == 'Quelque chose ne fonctionne pas'    Should be equal as strings    ${sf_subcategory}    Functional Issues
    Run keyword if    '${reason_option}' == 'Autre'    Should be equal as strings    ${sf_subcategory}   	Other Questions
    Run keyword if    '${category_option}' == 'Produits'    Should be equal as strings    ${sf_category}    Products
    Run keyword if    '${reason_option}' == 'Informations sur les produits'    Should be equal as strings    ${sf_subcategory}    Product information
    Run keyword if    '${reason_option}' == 'Demander un logo'    Should be equal as strings    ${sf_subcategory}    	Logo
    Run keyword if    '${reason_option}' == 'Autre'    Should be equal as strings    ${sf_subcategory}    Other Questions
    Run keyword if    '${category_option}' == 'Commandes'    Should be equal as strings    ${sf_category}    	Order
    Run keyword if    '${reason_option}' == 'Qualité'    Should be equal as strings    ${sf_subcategory}    	Quality
    Run keyword if    '${reason_option}' == 'Autre'    Should be equal as strings    ${sf_subcategory}    Other Questions
    Run keyword if    '${category_option}' == 'Autre'    Run keyword
    ...    Should be equal as strings    ${sf_category}    Other
    ...    AND    Should be equal as strings    ${sf_subcategory}    none

Map form with salesforce category NL
    Get SF case category
    Get SF case subcatgory
    Run keyword if    '${category_option}' == 'Over my.ccep.com'    Should be equal as strings    ${sf_category}   	Customer Portal
    Run keyword if    '${reason_option}' == 'Iets werkt niet'    Should be equal as strings    ${sf_subcategory}    Functional Issues
    Run keyword if    '${reason_option}' == 'Overige'    Should be equal as strings    ${sf_subcategory}   	Other Questions
    Run keyword if    '${category_option}' == 'Producten'    Should be equal as strings    ${sf_category}    Products
    Run keyword if    '${reason_option}' == 'Informatie over de producten'    Should be equal as strings    ${sf_subcategory}    Product information
    Run keyword if    '${reason_option}' == 'Vraag een logo aan'    Should be equal as strings    ${sf_subcategory}    	Logo
    Run keyword if    '${reason_option}' == 'Overige'    Should be equal as strings    ${sf_subcategory}    Other Questions
    Run keyword if    '${category_option}' == 'Bestellingen'    Should be equal as strings    ${sf_category}    	Order
    Run keyword if    '${reason_option}' == 'Kwaliteit'    Should be equal as strings    ${sf_subcategory}    	Quality
    Run keyword if    '${reason_option}' == 'Levering'    Should be equal as strings    ${sf_subcategory}    Question about delivery
    Run keyword if    '${reason_option}' == 'Voorraad'    Should be equal as strings    ${sf_subcategory}    Stock
    Run keyword if    '${reason_option}' == 'Overige'    Should be equal as strings    ${sf_subcategory}    Other Questions
    Run keyword if    '${category_option}' == 'Overige'    Run keyword
    ...    Should be equal as strings    ${sf_category}    Other
    ...    AND    Should be equal as strings    ${sf_subcategory}    none

Map request with salesforce category GB
    Get SF case category
    Get SF case subcatgory
    Run keyword if     '${chosen_reason}' == 'Other questions'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Order
    ...    AND    Should be equal as strings    ${sf_subcategory}   	Other Questions

Map request with salesforce category BELUX
    Get SF case category
    Get SF case subcatgory
    Run keyword if     '${chosen_reason}' == 'Qualité'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Order
    ...    AND    Should be equal as strings    ${sf_subcategory}   	Quality
    Run keyword if     '${chosen_reason}' == 'Autre'    Run keywords
    ...    Should be equal as strings    ${sf_category}    Order
    ...    AND    Should be equal as strings    ${sf_subcategory}   	Other Questions

Get SF case category
    ${sf_category}    Selenium Library.Get text    xpath=//tbody/tr/td[9]/div    #xpath=//td[contains(@class, 'td-00NU0000003kSza')]/div
    Set global variable    ${sf_category}

Get SF case subcatgory
    ${sf_subcategory}    Selenium Library.Get text    xpath=//tbody/tr/td[10]/div    #xpath=//td[contains(@class, 'td-00NU0000003kT0B')]/div
    Set global variable    ${sf_subcategory}

Search for invited email
    :FOR    ${i}    IN RANGE    1    10
    \    Selenium Library.Input text    id=phSearchInput    ${invited_email}
    \    Selenium Library.Click element    id=searchButtonContainer
    \    ${status}    Run keyword and return status    Selenium Library.Wait until element is visible    xpath=//table[@summary='ECP Registration Requests']
    \    Exit for loop if    '${status}' == 'True'

Go to registration request for invited employee
    Selenium Library.Click element    xpath=//*[@id="ECP_Registration_Request__c_body"]/table/tbody/tr[2]/th/a
    Selenium Library.Wait until element contains    xpath=//h2[@class='mainTitle']    ECP Registration Request Detail

Remember registration token
    ${registration_token}    Selenium Library.Get text    id=00N0P000006eMei_ileinner
    Set global variable    ${registration_token}
