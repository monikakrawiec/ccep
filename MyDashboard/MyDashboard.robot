*** Settings ***
Resource  keywords_mydashboard.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP

Dashboard should be shown after logging in
    [tags]    BELUX    NL    FRANCE    GB
    Go to login page
    Log in to CCEP
    Verify user logged in
    Verify Dashboard shown after log in
    Verify Dashboard highlighted
    Run keyword if    '${usr}' == 'BELUX'    Belux set locale to FR

CC logo directs to Dashboard
    [tags]    BELUX    NL    FRANCE    GB
    Click CC logo in header
    Verify home page redirection for logged in user

Delivery date update
    [tags]    GB
    @{delivery_options}    Create list    Choose MDT option    Choose MPP option    Choose Bulk option
    :FOR    ${option}    IN    @{delivery_options}
    \    Edit delivery date
    \    Set current month in calendar
    \    Run Keyword    ${option}
    \    Update delivery date
    \    Remember calendar date
    \    Submit delivery date
    \    Verify delivery date saved

Validate delivery date calendar
    [tags]    GB
    Edit delivery date
    @{delivery_options}    Create list    Choose MDT option    Choose MPP option    Choose Bulk option
    :FOR    ${option}    IN    @{delivery_options}
    \    Run Keyword    ${option}
    \    Set current date
    \    Dates from the past are not available
    \    Verify next upcoming day not available for delivery
    #\    Verify first available delivery date
    Weekends not available for MDT option
    Close delivery edit window

MyDashboard top menu item directs to Dashboard
    [tags]    BELUX    NL    FRANCE    GB
    Choose Dashboard from top menu
    Verify home page redirection for logged in user

Validate dashboard elements positioning
    [tags]    BELUX    NL    FRANCE    GB
    Verify dashboard title visible
    Verify dashboard Account container
    Verify dashboard MyInvoices container
    Verify dashboard MyOrders container
    Verify dashboard MyRequests container
    Verify dashboard Ordering container
    Verify dashboard Feedback container
    Verify dashboard Brands container
    Verify dashboard News container
    Run keyword unless    '${usr}' == 'BELUX'    Verify MyInvoices container is one third of full width
    Run keyword unless    '${usr}' == 'IBERIA'    Verify MyOrders container is one third of full width
    Verify MyRequests container is one third of full width
    Verify Feedback container is one third of full width
    #Verify no MyInvoices container for France

Last 5 invoices are shown in MyInvoices
    [tags]    IBERIA    NL    FRANCE    GB
    Verify only 5 invoices shown
    Run keyword unless    '${usr}' == 'FRANCE'    Verify invoices section with no invoices
    Verify the most fresh invoices shown
    #If invoice number is longer than 12 digit, then ellipsis displayed

MyInvoices table content validation
    [tags]    IBERIA    NL    FRANCE    GB
    Invoice date should be in first column
    Invoice number should be in second column
    Invoice amount should be in third column
    Invoice status should be in forth column

Button directing to invoices overview visible
    [tags]    IBERIA    NL    FRANCE    GB
    Verify invoice overview button visible

Last 5 requests are shown in MyRequests
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Verify max 5 requests shown
    Verify requests section with no requests
    Verify the most fresh requests shown

Verify cut-off time shown in Dashboard
    #[tags]    NL    FRANCE    GB
    Verify cut-off info shown

End step
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Close Browser
