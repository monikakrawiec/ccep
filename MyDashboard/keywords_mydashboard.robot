*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Verify Dashboard shown after log in
    Selenium Library.Wait until element is visible    xpath=//div[contains(@class, 'dashboard view container')]
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'dashboard view container')]

Verify only 5 invoices shown
    Get invoices count in dashboard
    BuiltIn.Should Be True    ${invoices_count_dashboard} <= 5

Get invoices count in dashboard
    ${invoices_count_dashboard}    SeleniumLibrary.Get Element Count    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/tbody/tr
    Set global variable    ${invoices_count_dashboard}

Verify max 5 requests shown
    Get requests count in dashboard
    BuiltIn.Should Be True    ${requests_count_dashboard} <= 5

Get requests count in dashboard
    ${requests_count_dashboard}    SeleniumLibrary.Get Element Count    xpath=//div[contains(@class, 'ecp-table request')]/form/table/tbody/tr
    Set global variable    ${requests_count_dashboard}

Verify the most fresh invoices shown
    Remember max 5 last invoices from MyInvoices
    Choose Dashboard from top menu
    Get invoice numbers from Dashboard
    Lists Should Be Equal    ${invoice_numbers}    ${invoice_numbers_dashboard}

Remember max 5 last invoices from MyInvoices
    Choose MyInvoices from top menu
    Reset all filters
    Remember 5 last invoice numbers from MyInvoices

Remember max 5 last requests from MyRequests
    Choose MyRequests from top menu
    Remember 5 last request numbers from MyRequests

Reset all filters
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'ecp-chip mb-2 mb-0-md ecp-chip--reset')]
    Sleep    3s

Remember 5 last invoice numbers from MyInvoices
    Get number of all invoices
    Run keyword if    ${all_invoices} >= 5    More than 5 invoices available
    Run keyword if    ${all_invoices} < 5    Less than 5 invoices available

Remember 5 last request numbers from MyRequests
    Get number of all requests
    Run keyword if    ${all_requests} >= 5    More than 5 requests available
    Run keyword if    ${all_requests} < 5    Less than 5 requests available

More than 5 invoices available
    @{invoice_numbers}    Create List
    :FOR    ${i}    IN RANGE    1    6
    \    ${invoice_number}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table')]/form/table/tbody/tr[${i}]/td[3]/div/div/div/p
    \    Append to list    ${invoice_numbers}    ${invoice_number}
    Set global variable    @{invoice_numbers}

More than 5 requests available
    @{request_numbers}    Create List
    :FOR    ${i}    IN RANGE    1    6
    \    ${request_number}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table')]/form/table/tbody/tr[${i}]/td[1]/div/div/div/p
    \    Append to list    ${request_numbers}    ${request_number}
    Set global variable    @{request_numbers}

Less than 5 invoices available
    @{invoice_numbers}    Create List
    :FOR    ${i}    IN RANGE    1    ${all_invoices}+1
    \    ${invoice_number}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table')]/form/table/tbody/tr[${i}]/td[3]/div/div/div/p
    \    Append to list    ${invoice_numbers}    ${invoice_number}
    Set global variable    @{invoice_numbers}

Less than 5 requests available
    @{request_numbers}    Create List
    :FOR    ${i}    IN RANGE    1    ${all_requests}+1
    \    ${request_number}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table')]/form/table/tbody/tr[${i}]/td[1]/div/div/div/p
    \    Append to list    ${request_numbers}    ${request_number}
    Set global variable    @{request_numbers}

Get number of all invoices
    ${all_invoices}    SeleniumLibrary.Get Text     xpath=//div[contains(@class, 'ecp-table__info-page')]
    @{words}    String.Split String    ${all_invoices}    ${SPACE}
    Set global variable    ${all_invoices}    @{words}[-2]
    #${all_invoices}    SeleniumLibrary.Get Text    xpath=//ul[contains(@class, 'nav nav-tabs')]/li[1]/a
    #${all_invoices}    String.Fetch From Right    ${all_invoices}    (
    #${all_invoices}    String.Fetch From Left    ${all_invoices}    )
    #${all_invoices}    String.Strip String    ${all_invoices}
    #${all_invoices}    BuiltIn.Convert to number    ${all_invoices}
    #Set global variable    ${all_invoices}

Get number of all requests
    ${all_requests}    SeleniumLibrary.Get Text     xpath=//div[contains(@class, 'ecp-table__info-page')]
    @{words}    String.Split String    ${all_requests}    ${SPACE}
    Set global variable    ${all_requests}    @{words}[-2]

Get invoice numbers from Dashboard
    @{invoice_numbers_dashboard}    Create List
    ${invoice_numbers_count}    BuiltIn.Get Length    ${invoice_numbers}
    :FOR    ${i}    IN RANGE    1    ${invoice_numbers_count}+1
    \    ${invoice_number_dashboard}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/tbody/tr[${i}]/td[2]/div/div/div/p
    \    Append to list    ${invoice_numbers_dashboard}    ${invoice_number_dashboard}
    Set global variable    @{invoice_numbers_dashboard}

Get request numbers from Dashboard
    @{request_numbers_dashboard}    Create List
    ${request_numbers_count}    BuiltIn.Get Length    ${request_numbers}
    :FOR    ${i}    IN RANGE    1    ${request_numbers_count}+1
    \    ${request_number_dashboard}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'ecp-table request')]/form/table/tbody/tr[${i}]/td[2]/div/div/div/p
    \    Append to list    ${request_numbers_dashboard}    ${request_number_dashboard}
    Set global variable    @{request_numbers_dashboard}

Verify the most fresh requests shown
    Remember max 5 last requests from MyRequests
    Choose Dashboard from top menu
    Get request numbers from Dashboard
    Lists Should Be Equal    ${request_numbers}    ${request_numbers_dashboard}

Invoice date should be in first column
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '1')]    Fecha factura
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '1')]    Date de facturation
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '1')]    Factuur datum
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '1')]    Invoice Date

Invoice number should be in second column
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '2')]    N.º factura
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '2')]    Nr. de facture
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '2')]    Factuur nummer
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '2')]    Invoice no.

Invoice amount should be in third column
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '3')]    Importe
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '3')]    Montant
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '3')]    Bedrag
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '3')]    Amount

Invoice status should be in forth column
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '4')]    Fecha de vencimiento
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '4')]    Statut
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '4')]    Status
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'ecp-table invoice')]/form/table/thead/tr/th[contains(@aria-colindex, '4')]    Status

Verify invoices section with no invoices
    Run keyword if    ${invoices_count_dashboard} == 0    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'ecp-table invoice')]/div/div[contains(@class, 'ecp-table__status--empty')]

Verify requests section with no requests
    Run keyword if    ${requests_count_dashboard} == 0    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'ecp-table request')]/div/div[contains(@class, 'ecp-table__status--empty')]

Verify invoice overview button visible
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[3]/div/div[2]/a[contains(@href, '/myaccount/invoices')]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[4]/div/div[2]/a[contains(@href, '/myaccount/invoices')]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]/div/div[2]/a[contains(@href, '/myaccount/invoices')]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]/div/div[2]/a[contains(@href, '/myaccount/invoices')]

Verify dashboard title visible
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'dashboard view container')]/span[contains(@class, 'dashboard__title')]

Verify dashboard MyInvoices container
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[3]
    #Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[4]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]

Verify dashboard MyOrders container
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[2]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[2]
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[2]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[2]

Verify dashboard MyRequests container
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[4]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[4]
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[3]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[4]

Verify dashboard Ordering container
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[1]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[1]
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[1]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[1]

Verify dashboard Feedback container
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[5]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[4]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[5]
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[4]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[5]

Verify dashboard Brands container
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[6]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[5]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[6]
    Run keyword if    '${usr}' == 'FRNACE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[5]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[6]

Verify dashboard News container
    Run keyword if    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[2]/div[7]
    Run keyword if    '${usr}' == 'BELUX'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[6]
    Run keyword if    '${usr}' == 'NL'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[7]
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[6]
    Run keyword if    '${usr}' == 'GB'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div/div[7]

Verify dashboard Account container
    Run keyword unless    '${usr}' == 'IBERIA'    SeleniumLibrary.Element Should Be Visible    xpath=//*[@id="app"]/main/div[1]/div[1]/div/div

#Verify News container is full width
    #SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-12')]/div[contains(@class, 'widget dashboardnews justify-content-center')]

#Verify Brands container is full width
    #SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-12')]/div[contains(@class, 'widget dashboardbrands justify-content-center')]

Verify MyInvoices container is one third of full width
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-4')]/div/div[contains(@class, 'ecp-table invoice')]

Verify MyOrders container is one third of full width
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-4')]/div/div[contains(@class, 'ecp-table order')]

Verify MyRequests container is one third of full width
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-4')]/div/div[contains(@class, 'ecp-table request')]

Verify Feedback container is one third of full width
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-4')]/div[contains(@class, 'widget dashboard-feedback-widget')]

Verify no MyInvoices container for France
    Run keyword if    '${usr}' == 'FRANCE'    SeleniumLibrary.Element Should Not Be Visible    xpath=//div[contains(@class, 'py-3 dashboard__widget col-lg-4')]/div/div[contains(@class, 'ecp-table invoice')]

Edit outlet widget
    Selenium Library.Click Element    xpath=//a[contains(@data-testid, 'ccep-a-order-outlet-widget-delivery-edit')]
    Selenium Library.Wait until element is visible    id=MODAL_OUTLET_SELECTION___BV_modal_header_

Verify cart deletion message appears
    Run keyword if    '${usr}' == 'GB'    Selenium Library.Element text should be    xpath=//div[@class='alert alert-primary']    Your existing basket is not empty and will be cleared due to outlet change

Confirm outlet update
    Selenium Library.Click Element    xpath=//button[@data-testid='ccep-submit-outlet-select-modal']
    Selenium Library.Wait until element is not visible    id=MODAL_OUTLET_SELECTION___BV_modal_header_

Edit delivery date
    Selenium Library.Click Element    xpath=//a[contains(@data-testid, 'ccep-a-order-outlet-widget-date-edit')]
    Selenium Library.Wait until element is visible    id=MODAL_DELIVERY_DATE___BV_modal_content_
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-submit-delivery-date')]/div
    Sleep    3s

Update delivery date
    :FOR    ${i}    IN RANGE    1    30
    \    ${row}    BuiltIn.Evaluate    random.randint(1, 5)    modules=random
    \    ${column}    BuiltIn.Evaluate    random.randint(1, 7)    modules=random
    \    ${status}    Run keyword and return status    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[${column}]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px;']
    \    Run keyword if    '${status}' == 'True'   Selenium Library.Click element    css=.c-week:nth-child(${row}) > .c-day-popover:nth-child(${column}) .c-day-content > div
    \    Exit for loop if     '${status}' == 'True'
    Set global variable    ${row}
    Set global variable    ${column}

Remember calendar date
    Set current month in calendar
    ${calendar_date}    Selenium Library.Get text    css=.c-week:nth-child(${row}) > .c-day-popover:nth-child(${column}) .c-day-content > div
    ${calendar_date_len}    BuiltIn.Get length    ${calendar_date}
    Run keyword if    ${calendar_date_len} == 1    BuiltIn.Catenate    0    ${calendar_date}
    Run keyword if    ${calendar_date_len} == 1    Set global variable    ${calendar_date}
    Run keyword if    ${calendar_date_len} != 1    Set global variable    ${calendar_date}

Submit delivery date
    Selenium Library.Click Element    xpath=//button[contains(@data-testid, 'ccep-submit-delivery-date')]/div
    Selenium Library.Wait until element is not visible    id=MODAL_DELIVERY_DATE
    Sleep    5s

Verify delivery date saved
    ${dashboard_delivery_info}    Selenium Library.Get Text    css=.info-block:nth-child(2) .content-wrapper
    Set todays date
    ${calendar_date_len}    BuiltIn.Get length    ${calendar_date}
    Run keyword if    ${calendar_date_len} == 1    Add leading zero to day
    Run keyword if    ${calendar_date_len} == 1    Set global variable    ${calendar_date}
    Run keyword if    ${calendar_date_len} != 1    Set global variable    ${calendar_date}
    Set global variable    ${new_delivery_date}    ${calendar_date}/${mm}/${yyyy}
    BuiltIn.Should contain    ${dashboard_delivery_info}    ${new_delivery_date}

Add leading zero to day
    ${calendar_date}    BuiltIn.Catenate    SEPARATOR=    0    ${calendar_date}
    Set global variable       ${calendar_date}

Set todays date
    ${current_date}    DateTime.Get current date    result_format=%d-%m-%Y
    @{words}    String.Split String    ${current_date}    -
    Set global variable    ${dd}      @{words}[0]
    Set global variable    ${mm}    @{words}[1]
    Set global variable    ${yyyy}    @{words}[2]
    Run keyword if    ${calendar_date} < ${dd}    Date is from next month

Date is from next month
    ${mm}    BuiltIn.Convert to integer    ${mm}
    ${mm}    BuiltIn.Evaluate    ${mm}+1
    ${mm}    BuiltIn.Convert to string    ${mm}
    Set global variable    ${mm}
    ${mm_len}    BuiltIn.Get Length    ${mm}
    Run keyword if    '${mm_len}' == '1'    Add preceding zero to month
    Set global variable    ${mm}

Add preceding zero to month
    ${mm}    Catenate    SEPARATOR=    0    ${mm}
    Set global variable    ${mm}

Set current date
    ${current_date}    DateTime.Get current date    result_format=%Y-%m-%d
    Set global variable    ${current_date}

Dates from the past are not available
    Set current month in calendar
    @{words}    String.Split String    ${current_date}    -
    Set global variable    ${dd}      @{words}[2]
    Set calendar position for current date
    ${dd}    Convert to number    ${dd}
    ${current_row}    Evaluate    ${dd} / 7
    ${current_row}    MathOperations.Round up    ${current_row}
    ${current_row}    Convert to integer    ${current_row}
    Set global variable    ${current_row}
    Run keyword if    '${current_row}' > '1'    More than 1 row
    Run keyword if    '${current_row}' == '1'    Only 1 row
    #Run keyword unless    '${current_row}' == '0'    Last row

More than 1 row
    :FOR    ${i}    IN RANGE    1    ${current_row}
    \    Set global variable    ${row}    ${i}
    \    Verify dates from the past for full row
    \    ${row}    Evaluate    ${row} + 1

Only 1 row
     :FOR    ${i}    IN RANGE    1    2
     \    Set global variable    ${row}    ${i}
     \    Verify dates from the past for not full row

Last row
    Set global variable    ${row}    ${current_row}
    Verify dates from the past for not full row

Verify dates from the past for full row
    :FOR    ${column}    IN RANGE    1    8
    \    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[${column}]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']

Verify dates from the past for not full row
    Set calendar position for current date
    :FOR    ${column}    IN RANGE    1    ${current_column}+1
    \    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[${column}]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']

Verify next upcoming day not available for delivery
    # Sunday is last in the column, so validation depends on the current day position
    ${upcoming_day}    BuiltIn.Evaluate    ${current_column}+1
    ${upcoming_week}    BuiltIn.Evaluate    ${current_row}+1
    Run keyword unless    '${day}' == 'Sunday'    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${current_row}]/div[${upcoming_day}]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']
    Run keyword if    '${day}' == 'Sunday'    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${upcoming_week}]/div[1]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']

Set calendar position for current date
      ${day}    DateTime.Convert Date    ${current_date}    result_format=%A
      Set global variable     ${day}
      Run keyword if    '${day}' == 'Monday'    Set global variable    ${current_column}    1
      Run keyword if    '${day}' == 'Tuesday'    Set global variable    ${current_column}    2
      Run keyword if    '${day}' == 'Wednesday'    Set global variable    ${current_column}    3
      Run keyword if    '${day}' == 'Thursday'    Set global variable    ${current_column}    4
      Run keyword if    '${day}' == 'Friday'    Set global variable    ${current_column}    5
      Run keyword if    '${day}' == 'Saturday'    Set global variable    ${current_column}    6
      Run keyword if    '${day}' == 'Sunday'    Set global variable    ${current_column}    7

Verify first available delivery date

Set delivery to MDT
    Choose Dashboard from top menu
    Edit delivery date
    Choose MDT option
    Submit delivery date

Set delivery to Bulk
    Choose Dashboard from top menu
    Edit delivery date
    Choose Bulk option
    Submit delivery date

Choose MDT option
    Selenium Library.Click element    xpath=//span[contains(.,'MDT')]
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-submit-delivery-date')]/div
    Sleep    3s

Choose MPP option
    Selenium Library.Click element    xpath=//span[contains(.,'MPP')]
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-submit-delivery-date')]/div
    Sleep    3s

Choose Bulk option
    Selenium Library.Click element    xpath=//span[contains(.,'Bulk')]
    Selenium Library.Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-submit-delivery-date')]/div
    Sleep    3s

Weekends not available for MDT option
    Choose MDT option
    Count calendar rows
    Verify weekends not available for MDT

Count calendar rows
    ${calendar_rows}    Selenium Library.Get Element Count    xpath=//div[@class='c-weeks-rows']/div[@class='c-week']
    Set global variable    ${calendar_rows}

Verify weekends not available for MDT
    :FOR    ${row}    IN RANGE    1    ${calendar_rows}+1
    \    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[6]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']
    \    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[7]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px; color: rgb(217, 140, 140); font-weight: 600; opacity: 0.6; border-radius: 0px;']

Close delivery edit window
    Selenium Library.Click element    css=#MODAL_DELIVERY_DATE___BV_modal_header_ .fa

Set delivery date to next available date
    Edit delivery date
    Update delivery date
    Remember calendar date
    Set current month in calendar
    Choose next available delivery date
    Submit delivery date

Set delivery date to last available date
    Edit delivery date
    Choose last available delivery date
    Submit delivery date

Get week of current date
    Set todays date
    ${start_row}    BuiltIn.Evaluate    ${dd} / 7.0
    ${start_row}    MathOperations.Round up    ${start_row}
    ${start_row}    BuiltIn.Convert to integer    ${start_row}
    Set global variable    ${start_row}

Choose next available delivery date
    Get week of current date
    :FOR    ${row}    IN RANGE    ${start_row}    7    #iterates through calendar rows
    \    Set global variable    ${row}
    \    Iterate through calendar week
    \    Exit for loop if     '${column_available}' == 'True'
    Sleep    3s

Iterate through calendar week
    # checks within one week if there is a day that fulfils condition
    :FOR    ${column}    IN RANGE    1    8
    \    ${column_available}    Run keyword and return status    Selenium Library.Element should be visible    xpath=//div[@class='c-week'][${row}]/div[${column}]/div[@class='c-day']/div[@class='c-day-content-wrapper']/div[@style='width: 30px; height: 30px;']
    \    Run keyword if    '${column_available}' == 'True'   Selenium Library.Click element    css=.c-week:nth-child(${row}) > .c-day-popover:nth-child(${column}) .c-day-content > div
    \    Exit for loop if     '${column_available}' == 'True'
    Set global variable    ${column_available}

Choose last available delivery date
    @{reversed_rows}    Create List    7    6    5    4    3    2    1
    :FOR    ${row}    IN    1    6    @{reversed_rows}
    \    Set global variable    ${row}
    \    Iterate through calendar week
    \    Exit for loop if     '${column_available}' == 'True'
    Sleep    3s

Set current month in calendar
    Run keyword and ignore error    Selenium Library.Click Element    css=.c-arrow-layout:nth-child(1) > .svg-icon
    Sleep    3s

Verify cut-off info shown
    Selenium Library.Element should be visible    xpath=//span[@class='content-wrapper__cutoff']
