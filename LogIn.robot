*** Settings ***
Library  SeleniumLibrary    15
Resource  keywords.robot

*** Variables ***
${SITE_URL}    https://ecpt-serviceportal.cs108.force.com
${SELENIUM_TIME_OUT}    120
${SELENIUM_SPEED}    0.45
${BSUser}   monikakrawiec3
${AccessKey}   9h7NFAFG139W1C582Mp9
${RemoteUrl}   http://${BSUser}:${AccessKey}@hub.browserstack.com/wd/hub
${DC}    os:ios,browser:iPhone,realMobile:true,deviceOrientation:landscape
#${DC}    device:iPhone 8 Plus,real_mobile:false
#desired_capabilities=browser:${BROWSER},browser_version:${BROWSER_VERSION},os:${OS},os_version:${OS_VERSION}
#os:MAC, WIN8, XP, WINDOWS, ANY, ANDROID


**Test Cases**

Case1: Successful login
    #Open Browser    ${SITE_URL}    remote_url=${RemoteUrl}    desired_capabilities=${DC}
    Open ECP testing
    Maximize Browser Window
    Go to login page
    Log in to ECPT
    Verify user logged in
    Close Browser

Case2: Unsuccessful login
    #Open Browser    ${SITE_URL}    ${BROWSER}  remote_url=${RemoteUrl}  desired_capabilities=${DC}
    Open ECP testing
    Maximize Browser Window
    Go to login page
    Log in to ECPT with invalid data
    Verify user not logged in
    Close Browser
