*** Settings ***
Resource  keywords_checkout.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB
    #Open Browser    ${SITE_URL}    ${browser}  remote_url=${RemoteUrl}  desired_capabilities=${DC}
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in

Make a successful order BELUX NL
    #[tags]    BELUX
    Belux set locale to NL
    Show all products
    Choose random product
    #Choose palet option
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${quantity_value}    150
    Set quantity
    Add to cart
    Click cart icon
    Remember basket items
    Set global variable    @{cart_products_order}    @{cart_products}
    Process to checkout
    Remember checkout items
    Remember items quantity
    Verify Buy button grayed out
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
    Remember delivery date

Make a successful order
    [tags]    BELUX    NL    FRANCE    GB
    Run keyword if    '${usr}' == 'BELUX'    Belux set locale to FR
    Show all products
    Choose random product
    #Choose palet option
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${quantity_value}    150
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${quantity_value}    3000
    Set quantity
    Add to cart
    Click cart icon
    Remember basket items
    Set global variable    @{cart_products_order}    @{cart_products}
    Process to checkout
    Remember checkout items
    Remember items quantity
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
    Remember delivery date
    Verify day corresponds with date

Validate order in SalesForce
    [tags]    BELUX    NL    FRANCE    GB
    Go to Salesforce
    Log in to SalesForce
    Open SalesForce classic
    Search for order
    Submit order search
    Enter details of order
    Remember salesforce items
    Remember salesforce items quantity
    Compare checkout items with salesforce order
    Verify product quantities saved correctly

Reorder previous order
    [tags]    BELUX    NL    FRANCE    GB
    Go to CCEP
    Edit delivery date
    Update delivery date
    Remember calendar date
    Submit delivery date
    Verify delivery date saved
    Choose ShopProducts from top menu
    Choose MyOrders from top menu
    Verify order stored in MyOrders
    Reorder last order
    #Confirm reorder
    Remember basket items
    Set global variable    @{cart_products_reorder}    @{cart_products}
    Verify all products added to cart from original order
    Process to checkout
    Select Terms&conditions checkbox
    Order now
    Verify checkout success page
    Remember order number
    Remember delivery date
    Run keyword if    '${usr}' == 'NL'    Adjust date display
    Verify delivery date changed
    Verify day corresponds with date

Reorder with not empty cart

Cancel reorder
    #[tags]    BELUX    NL    FRANCE    GB
    Choose MyOrders from top menu
    Cancel last reorder
    Confirm order cancelation
