*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Select Terms&conditions checkbox
    Execute Javascript    (function() {document.querySelector("#app > main > section > div > div > form > div:nth-child(3) > div:nth-child(2) > div > div > label").click();})();

Order now
    SeleniumLibrary.Click Element    css=.mt-4 > button:nth-child(1) > div:nth-child(1)
    Selenium Library.Wait until element is visible    xpath=//section[@class='order-confirmation']
    Selenium Library.Wait until element is visible    xpath=//div[@class='thankyou text-center']

Verify checkout success page
    Selenium Library.Element should be visible    xpath=//section[@class='order-confirmation']

Remember order number
    ${order_number}    Selenium Library.Get text    xpath=//div[@class='thankyou__content']/p[1]/strong
    ${order_number}    String.Fetch from left    ${order_number}    .
    Set global variable    ${order_number}

Verify all products added to cart from original order
    Collections.Lists should be equal    ${cart_products_order}    ${cart_products_reorder}

Remember checkout items
    Get products count from checkout
    @{checkout_products}    Create List
    :FOR    ${i}    IN RANGE    2    ${checkout_products_count}+1    # counts from 2 because first div is a container title
    \    ${name}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'cart')]/div[contains(@class, 'row')][${i}]/div/div[1]/div/div[1]/div/span
    \    Collections.Append To List    ${checkout_products}    ${name}
    Set Global Variable    @{checkout_products}

Get products count from checkout
    ${checkout_products_count}    SeleniumLibrary.Get Element Count    xpath=//div[contains(@class, 'cart')]/div
    Set Global Variable    ${checkout_products_count}

Remember items quantity
    Selenium Library.Wait until element is visible    xpath=//div[@class='productlist-item__roundings--bold']
    @{checkout_items_quantity}    Create List
    :FOR    ${i}    IN RANGE    2    ${checkout_products_count}+1    # counts from 2 because first div is a container title
    \    ${quantity}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'cart')]/div[${i}]/div/div[2]/div
    \    Collections.Append To List    ${checkout_items_quantity}    ${quantity}
    Set Global Variable    @{checkout_items_quantity}

Remember delivery date
    ${delivery_info}    Selenium Library.Get text     xpath=//h3[@class='text-center thankyou__subline']
    @{words}    String.Split String    ${delivery_info}    ${SPACE}
    Set global variable    ${delivery_date}   @{words}[-1]
    Set global variable    ${delivery_day}    @{words}[-2]

Adjust date display
    @{letters}    String.Split string to characters    ${mm}
    Run keyword if    '@{letters}[0]' == '0'    Set Global Variable    ${mm}    @{letters}[1]
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${new_delivery_date}    ${calendar_date}-${mm}-${yyyy}

Verify delivery date changed
    BuiltIn.Should be equal as strings    ${delivery_date}    ${new_delivery_date}    # delivery_date taken from dashboard

Extract weekday from date
    Run keyword if    '${usr}' == 'GB' or '${usr}' == 'BELUX'    Set global variable    ${split_element}    /
    Run keyword if    '${usr}' == 'NL'    Set global variable    ${split_element}    -
    @{words}    String.Split String    ${delivery_date}    ${split_element}
    Set global variable    ${dd}      @{words}[0]
    Set global variable    ${mm}    @{words}[1]
    Set global variable    ${yyyy}    @{words}[2]
    Run keyword if    '${usr}' == 'NL' or '${usr}' == 'BELUX'    Convert to 2 digit format
    Set global variable    ${delivery_date}    ${yyyy}-${mm}-${dd}
    ${day}    DateTime.Convert Date    ${delivery_date}    result_format=%A
    Set global variable    ${day}


Convert to 2 digit format
    ${dd_len}    BuiltIn.Get Length    ${dd}
    Run keyword if    ${dd_len} == 1    Set global variable    ${dd}    0${dd}
    ${mm_len}    BuiltIn.Get Length    ${mm}
    Run keyword if    ${mm_len} == 1    Set global variable    ${mm}    0${mm}

Verify day corresponds with date
    Extract weekday from date
    Run keyword if    '${day}' == 'Thursday'    Verify Thursday delivery date
    Run keyword if    '${day}' == 'Friday'    Verify Friday delivery date
    Run keyword if    '${day}' == 'Saturday'    Verify Saturday delivery date
    Run keyword if    '${day}' == 'Sunday'    Verify Sunday delivery date
    Run keyword if    '${day}' == 'Monday'    Verify Monday delivery date
    Run keyword if    '${day}' == 'Tuesday'    Verify Tuesday delivery date
    Run keyword if    '${day}' == 'Wednesday'    Verify Wednesday delivery date

Verify Thursday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Thu
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    jeu.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    do.

Verify Friday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Fri
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    ven.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    vr.

Verify Saturday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Sat
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    sam.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    zat.

Verify Sunday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Sun
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    dim.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    zon.

Verify Monday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Mon
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    lun.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    maa.

Verify Tuesday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Tue
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    mar.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    di.

Verify Wednesday delivery date
    Run keyword if    '${usr}' == 'GB'    BuiltIn.Should be equal as strings    ${delivery_day}    Wed
    Run keyword if    '${usr}' == 'BELUX'    BuiltIn.Should be equal as strings    ${delivery_day}    mer.
    Run keyword if    '${usr}' == 'NL'    BuiltIn.Should be equal as strings    ${delivery_day}    wo.

Edit cart
    Selenium Library.Click element    xpath=//a[contains(@data-testid, 'ccep-a-step-card-edit')]
    Selenium Library. Wait until element is visible    xpath=//button[contains(@data-testid, 'ccep-button-step-cart-clear')]

Verify Buy button grayed out
    Selenium Library.Element attribute value should be    xpath=//button[contains(@data-testid, 'ccep-submit-order-complete')]    class    btn btn-secondary

Verify order stored in MyOrders
    Selenium Library.Page should contain    ${order_number}
