*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Click on 'Already have account' from home page
    SeleniumLibrary.Click Element     xpath=//div[contains(@class, 'landing')]/p/a[contains(@href, '/login#login')]

Verify user redirected to login page
    SeleniumLibrary.Location Should Be     ${site_url}/login#login
    SeleniumLibrary.Wait Until Page Contains Element    id=ecp-input-login-email

Verify login tab opened
    Sleep    3s
    SeleniumLibrary.Element Should Be Visible    id=ecp-input-login-email

Back to home page breadcrumb should be visible
    SeleniumLibrary.Element Should Be Visible    xpath=//a[contains(@class, 'breadcrumbs__link')]

Click breadcrumb on login page
    SeleniumLibrary.Click Element    css=.breadcrumbs__link
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'container--login col-lg-8 col-12')]/h1

Verify user redirected to home page
    SeleniumLibrary.Location Should Be    ${site_url}/

Insert valid login data
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-email    ${user}
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-email    ${user}
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-password    ${ccep_password}
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-password    ${password}

Log in to CCEP
    Insert valid login data
    SeleniumLibrary.Click Element    xpath=//button[contains(@data-testid, 'ccep-submit-login')]
    Sleep    10s

Log in to CCEP with invalid credentials combination
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-email    ${user}
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-email    ${user}
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-password    password
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-password    password
    SeleniumLibrary.Click Element    xpath=//button[contains(@data-testid, 'ccep-submit-login')]
    Sleep    5s

Log in to CCEP with unexisting credentials
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-email    user@gmail.com
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-email    user@gmail.com
    Run keyword if    '${env}' == 'ecpt'    SeleniumLibrary.Input Text    id=ecp-input-login-password    password
    Run keyword if    '${env}' == 'int'    SeleniumLibrary.Input Text    id=ecp-input-password    password
    SeleniumLibrary.Click Element    xpath=//button[contains(@data-testid, 'ccep-submit-login')]
    Sleep    5s

Log off
    Click user icon
    SeleniumLibrary.Click Element    xpath=(//a[contains(@href, '/secur/logout.jsp')])[2]
    Sleep    5s
    SeleniumLibrary.Wait Until Page Contains    Welcome back to My CCEP
    Verify user redirected to home page

Verify user logged in
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'dashboard view container')]
    ${current_location}   SeleniumLibrary.Log Location
    BuiltIn.Should Be Equal As Strings    ${current_location}    ${site_url}/home

Verify user not logged in
    SeleniumLibrary.Page Should Contain Element    xpath=//div[contains(@class, 'alert alert-danger')]

Verify invalid credentials error message
    SeleniumLibrary.Element Text Should Be    xpath=//div[contains(@class, 'alert alert-danger')]    Your login attempt has failed. Make sure the username and password are correct.

Open sing-up tab
    SeleniumLibrary.Click Element    xpath=//ul[contains(@class, 'nav nav-tabs')]/li[1]

Verify sign-up tab opened
    Sleep    3s
    SeleniumLibrary.Element Should Be Visible    id=ecp-input-signup-firstname

Click on 'Already have account' link from sign-up tab
    SeleniumLibrary.Click Element    xpath=//form[contains(@id, 'signup-form')]/div[contains(@class, 'text-md-left signup-link')]/a

Verify password hidden by default
    SeleniumLibrary.Element Should Be Visible    xpath=//span[contains(@class, 'fa fa-lg fa-fw input-field__toggle-password fa-eye-slash')]

Show password
    SeleniumLibrary.Click Element    xpath=//span[contains(@class, 'fa fa-lg fa-fw input-field__toggle-password fa-eye-slash')]

Verify password shown
    SeleniumLibrary.Element Should Be Visible     xpath=//span[contains(@class, 'fa fa-lg fa-fw input-field__toggle-password fa-eye')]
