*** Settings ***
Resource  keywords_login.robot

**Test Cases**

Successful login
    [tags]    BELUX    NL    FRANCE    GB
    #Open Browser    ${SITE_URL}    remote_url=${RemoteUrl}    desired_capabilities=${DC}
    Open CCEP
    Go to login page
    Back to home page breadcrumb should be visible
    Log in to CCEP
    Verify user logged in
    Log off

Unsuccessful login with invalid credentials combination
    [tags]    BELUX    NL    FRANCE    GB
    Go to login page
    Log in to CCEP with invalid credentials combination
    Verify user not logged in
    Verify invalid credentials error message

Unsuccessful login with unexisting credentials
    [tags]    BELUX    NL    FRANCE    GB
    Log in to CCEP with unexisting credentials
    Verify user not logged in
    Verify invalid credentials error message

Breadcrumb on login page redirects to home page
    [tags]    BELUX    NL    FRANCE    GB
    Click breadcrumb on login page
    Verify user redirected to home page
    Close browser

Clicking on 'Already have an account' from home redirects to login
    [tags]    BELUX    NL    FRANCE    GB
    Open CCEP
    Click on 'Already have account' from home page
    Verify user redirected to login page
    Verify login tab opened

Clicking on 'Already have an account' from sign-up tab redirects to login
    [tags]    BELUX    NL    FRANCE    GB
    Open sing-up tab
    Verify sign-up tab opened
    Click on 'Already have account' link from sign-up tab
    Verify login tab opened

Password hidden by default
    [tags]    BELUX    NL    FRANCE    GB
    Insert valid login data
    Verify password hidden by default
    Show password
    Verify password shown
    Close browser
