*** Settings ***
Resource  keywords_plp.robot

**Test Cases**

Prerequisites
    [tags]    BELUX    NL    FRANCE    GB    xxx
    Open CCEP
    Go to login page
    Log in to CCEP
    Verify user logged in
    Show all products

Validate shop container tabs
    [tags]    BELUX    NL    FRANCE    GB
    Choose MyOrders tab from PLP
    Verify MyOrders tab opened
    Choose MyLists tab from PLP
    Verify MyLists tab opened
    Choose AllProducts tab from PLP
    Verify AllProducts tab opened

Validate quantity picker
    [tags]    BELUX    NL    FRANCE    GB
    Set test variable    ${quantity_value}    0
    Remember random buyable product from PLP
    Insert quantity value PLP
    Verify input field highlighted with error PLP
    Verify add to cart button disabled PLP
    Verify quantity increase disabled PLP
    Verify quantity decrease disabled PLP
    Set test variable    ${quantity_value}    1
    Insert quantity value PLP
    Verify input field not highlighted with error
    Verify add to cart button enabled PLP
    Verify quantity increase enabled PLP
    Verify quantity decrease disabled PLP
    Set test variable    ${quantity_value}    2
    Insert quantity value PLP
    Verify input field not highlighted with error PLP
    Verify quantity increase enabled PLP
    Verify quantity decrease enabled PLP

Add to cart from PLP
    [tags]    BELUX    NL    FRANCE    GB
    Remember random buyable product from PLP
    Add to cart from PLP
    Verify product added to cart from PLP

Validate pagination
    [tags]    BELUX    NL    FRANCE    GB
    Get products quantity from PLP
    Count products on listing page
    Should be equal as numbers    ${plp_products_count}    ${products_on_page}
    Get total number of products
    Run keyword if    ${total_products_quantity} > 18    #    next steps will run only if there are more plp pages
    ...    Run keywords
    ...    Get current pagination settings
    ...    AND    Get total number of products
    ...    AND    Get number of available pages
    ...    AND    Verify number of pages reflects pagination settings
    ...    AND    Show 36 products per page
    ...    AND    Get products quantity from PLP
    ...    AND    Count products on listing page
    ...    AND    Should be equal as numbers    ${plp_products_count}    ${products_on_page}

Validate product filters BELUX
    #[tags]    BELUX
    Get all PLP products count
    Set global variable    ${all_products_count}    ${plp_products_count}
    Verify product filter positioning BELUX
    Brand filter unfold by default BELUX
    #Flavour filter collapsed by default
    Size filter collapsed by default BELUX
    Packaging filter collapsed by default BELUX
    Product Label filter collapsed by default BELUX

Verify products filtered and chip shown for active filter
    [tags]    BELUX
    Get all PLP products count
    Set global variable    ${all_products_count}    ${plp_products_count}
    Unfold brand filter
    Count brand elements
    Choose random brand
    Set global variable    ${chosen_brand_name1}    ${chosen_brand_name}
    Set global variable    ${chosen_brand_quantity1}    ${chosen_brand_quantity}
    Verify Brand chip present in products overview
    Verify Brand products filtered

Validate OR condition for Brand filter BElUX
    [tags]    BELUX
    :FOR    ${i}    IN RANGE    1    ${brand_elements}
    \    Choose random brand
    \    Remember chosen brand name
    \    Exit for loop if    '${chosen_brand_name}' != '${chosen_brand_name1}'
    Remember items quantity for chosen brand
    Verify Brand chip present in products overview
    Verify OR condition applied for Brand filter

Validate filters removal
    [tags]    BELUX
    Remove filter by removing chip
    Verify filter removed
    Remove all filters
    Get all PLP products count
    BuiltIn.Should be equal as integers    ${all_products_count}    ${plp_products_count}

Validate OR condition for Size filter BElUX
    [tags]    BELUX
    Unfold size filter
    Count size elements
    Choose random size
    Set global variable    ${chosen_size_name1}    ${chosen_size_name}
    Set global variable    ${chosen_size_quantity1}    ${chosen_size_quantity}
    Verify Size chip present in products overview
    Verify Size products filtered
    :FOR    ${i}    IN RANGE    1    ${size_elements}
    \    Choose random size
    \    Exit for loop if    '${chosen_size_name}' != '${chosen_size_name1}'
    Verify Size chip present in products overview
    Verify OR condition applied for Size filter
    Remove all filters

Validate AND condition between filters
    [tags]    BELUX
    Count size elements
    Choose random size
    Unfold packaging filter
    Count packaging elements
    Choose random packaging
    Verify Size chip present in products overview
    Verify Packaging chip present in products overview
    Verify AND condition applied

Validate product filters GB
    [tags]    GB
    Size filter collapsed by default GB
    Unfold size filter
    Count size elements
    Choose random size
    Verify Size chip present in products overview
    Verify Size products filtered

Validate text filter
    [tags]    GB    NL    BELUX    IBERIA    xxx
    Show all products
    Set global variable    ${search_term}    250
    Set global variable    ${first_search_term}    ${search_term}
    Insert search term into text filter
    Apply text filter
    Verify chip present in products overview
    Show 72 products per page
    Verify filter applied
    Set global variable    ${search_term}    Bottle
    Insert search term into text filter
    Apply text filter
    Verify chip present in products overview
    Verify filter applied
    Set global variable    ${search_term}    ${first_search_term}
    Verify chip present in products overview
    Verify filter applied
    Remember random product
    Set global variable    ${single_product_name}    ${product_name}
    Set global variable    ${single_product_ean}    ${product_ean}
    Set global variable    ${search_term}    ${product_ean}
    Insert search term into text filter
    Apply text filter
    Verify chip present in products overview
    Verify only 1 product shown in search results
    Remove all filters
    Set global variable    ${search_term}    xxxxxx
    Insert search term into text filter
    Apply text filter
    Verify chip present in products overview
    Verify empty search results


Final step
    [tags]    IBERIA    BELUX    NL    FRANCE    GB
    Close browser
