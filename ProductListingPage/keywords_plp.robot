
*** Settings ***
Resource         ../keywords.robot

*** Keywords ***

Show all products
    Selenium Library.Go to    ${site_url}/shop
    Sleep    5s

Get products quantity from PLP
    #counts all products on 1 page
    ${products_quantity}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'products-overview__header-count')]/span
    @{words}    String.Split String    ${products_quantity}    ${SPACE}
    Set global variable    ${plp_products_count}    @{words}[2]

Get all PLP products count
    #counts products on all pages
    ${products_quantity}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'products-overview__header-count')]/span
    @{words}    String.Split String    ${products_quantity}    ${SPACE}
    Set global variable    ${plp_products_count}    @{words}[-2]

Choose random product
    #Get products quantity from PLP
    #${random_int}    Evaluate    random.randint(1, ${plp_products_count})    modules=random
    #Set Global Variable    ${random_int}
    #Remember product name
    Remember random buyable product from PLP
    Go to PDP of remembered product

Go to PDP of remembered product
    SeleniumLibrary.Click Element    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random_int}]/div/div[1]/div[1]/a
    SeleniumLibrary.Wait Until Page Contains Element    xpath=//div[contains(@class, 'product-detail view container')]

Remember random buyable product from PLP
    Get products quantity from PLP
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}
    \    ${random_int}    BuiltIn.Evaluate    random.randint(1, ${plp_products_count})    modules=random
    \    Set global variable    ${random_int}
    \    ${status}   Run Keyword And Return Status    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block')]   # checks if buy button is visibl
    \    Run keyword if    '${status}' == 'True'    Set global variable    ${random_int}
    \    Exit For Loop If    '${status}' == 'True'
    Remember product name

Remember random product
    Get products quantity from PLP
    ${random_int}    BuiltIn.Evaluate    random.randint(1, ${plp_products_count})    modules=random
    Remember product name
    Remember product EAN

Remember product EAN
    ${product_ean}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[1]/span[2]
    ${product_ean}    String.Fetch from right    ${product_ean}    :
    ${product_ean}    String.Strip string    ${product_ean}
    Set global variable    ${product_ean}

Remember product name
    ${product_name}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[1]/div[1]/a
    Set global variable    ${product_name}

Choose buyable product with Case unit
    Get products quantity from PLP
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}
    \    ${has_case_unit}    Selenium Library.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[1]/button[@class='btn unit-switch__unit btn-block btn-primary']
    \    Exit for loop if    '${has_case_unit}' == 'Case'
    Set global variable    ${i}
    Set global variable    ${random_int}    ${i}
    Go to PDP of remembered product

Choose buyable product with Colli unit
    Get products quantity from PLP
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}
    \    ${has_case_unit}    Selenium Library.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[1]/button[@class='btn unit-switch__unit btn-block btn-primary']
    \    Exit for loop if    '${has_case_unit}' == 'Colli'
    Set global variable    ${i}
    Set global variable    ${random_int}    ${i}
    Go to PDP of remembered product

Has case unit BELUX
    ${has_case_unit}    Selenium Library.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[1]/button[@class='btn unit-switch__unit btn-block btn-primary']
    Set global variable    ${has_case_unit}

Has case unit GB
    ${has_case_unit}    Selenium Library.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[1]/button[@class='btn unit-switch__unit btn-block btn-primary']
    Set global variable    ${has_case_unit}

Choose buyable product with Layer unit
    Get products quantity from PLP
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}
    \    ${has_layer_unit}    Run Keyword And Return Status    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[2]/button[@class='btn unit-switch__unit btn-block btn-secondary']
    \    Exit for loop if    '${has_layer_unit}' == 'True'
    Set global variable    ${i}
    Set global variable    ${random_int}    ${i}
    Go to PDP of remembered product

Choose buyable product with Unite unit
    Get products quantity from PLP
    :FOR    ${i}    IN RANGE    1    ${plp_products_count}
    \    Set global variable    ${i}
    \    Run keyword and ignore error    Check if has Unite unit
    \    Exit for loop if    '${has_unite_unit}' == 'Unité(s)'
    Set global variable    ${i}
    Set global variable    ${random_int}    ${i}
    Go to PDP of remembered product

Check if has Unite unit
    ${has_unite_unit}    Selenium Library.Get Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${i}]/div/div[3]/div[2]/div/div[1]/div[1]/button[@class='btn unit-switch__unit btn-block btn-primary']
    Set Global Variable    ${has_unite_unit}

Choose MyOrders tab from PLP
    SeleniumLibrary.Click Element    id=tab-myorders___BV_tab_button__
    Selenium Library.Wait until element is visible    id=tab-myorders

Choose MyLists tab from PLP
    SeleniumLibrary.Click Element    id=tab-mylists___BV_tab_button__
    Selenium Library.Wait until element is visible    id=tab-mylists

Choose AllProducts tab from PLP
    SeleniumLibrary.Click Element    id=tab-products___BV_tab_button__
    Selenium Library.Wait until element is visible    id=tab-products

Verify MyOrders tab opened
    SeleniumLibrary.Location should be    ${site_url}/shop
    SeleniumLibrary.Element Should Be Visible    id=tab-myorders

Verify MyLists tab opened
    SeleniumLibrary.Location should be    ${site_url}/shop
    SeleniumLibrary.Element Should Be Visible    id=tab-mylists

Verify AllProducts tab opened
    SeleniumLibrary.Location should be    ${site_url}/shop
    SeleniumLibrary.Element Should Be Visible    id=tab-products

Add to cart from PLP
    Selenium Library.Set focus to element    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block')]/span
    Selenium Library.Click Element    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block')]/span

Verify product added to cart from PLP
    Verify green check added to buy button
    Verify basket icon quantity

Verify green check added to buy button
    SeleniumLibrary.Element Should Be Visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button/div[contains(@class, 'added-block')]

Verify basket icon quantity
    SeleniumLibrary.Element Should Be Visible    xpath=//span[contains(@class, 'cart__badge cart__badge--primary cart__badge--circle')]

Verify add to cart button enabled PLP
    SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block disabled')]
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block')]

Verify add to cart button disabled PLP
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[3]/button[contains(@class, 'btn btn-primary btn-block disabled')]

Verify quantity increase enabled PLP
    #SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[1]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3 disabled')]
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3')]

Verify quantity decrease enabled PLP
    #SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[1]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3 disabled')]
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3')]

Verify quantity increase disabled PLP
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3 disabled')]
    #SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-increase col-3')]

Verify quantity decrease disabled PLP
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3 disabled')]
    #SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/button[contains(@class, 'btn btn-secondary ecp-quantity__action ecp-quantity__action-decrease col-3')]

Get current pagination settings
    #${current_pagination_setting}    Selenium Library.Get Text    xpath=//div[@class='col d-none d-md-flex col-md-6 col-lg justify-content-end']/div/button
    ${current_pagination_setting}    Selenium Library.Get Text    xpath=//div[2]/div/div[4]/div/div[3]/div/button
    #Run keyword if    '${usr}' == 'BELUX'    Get current pagination settings Belux
    #Run keyword if    '${usr}' == 'GB'    Get current pagination settings GB
    #Run keyword if    '${usr}' == 'NL'    Get current pagination settings Nl
    @{words}    String.Split String    ${current_pagination_setting}    ${SPACE}
    Set global variable    ${current_pagination_setting}    @{words}[1]

Get current pagination settings Belux
    #${current_pagination_setting}    Selenium Library.Get Text    css=#__BVID__279__BV_toggle_
    ${current_pagination_setting}    Selenium Library.Get Text    xpath=//div[@class='col d-none d-md-flex col-md-6 col-lg justify-content-end']/div/button
    Set global variable    ${current_pagination_setting}

Get current pagination settings GB
    ${current_pagination_setting}    Selenium Library.Get Text    css=#__BVID__270__BV_toggle_
    Set global variable    ${current_pagination_setting}

Get current pagination settings Nl
    ${current_pagination_setting}    Selenium Library.Get Text    css=#__BVID__267__BV_toggle_
    Set global variable    ${current_pagination_setting}

Count products on listing page
    ${products_on_page}    Selenium Library.Get Element Count    xpath=//div[contains(@class, 'products-overview__container')]/div/div
    Set global variable    ${products_on_page}

Get total number of products
    ${total_products_quantity}    SeleniumLibrary.Get Text    xpath=//div[contains(@class, 'products-overview__header-count')]/span
    @{words}    String.Split String    ${total_products_quantity}    ${SPACE}
    Set global variable    ${total_products_quantity}    @{words}[-2]

Get number of available pages
    ${available_pages}    SeleniumLibrary.Get Text   xpath=//div[2]/div/div[4]/div/div[2]/div/div/button
    Set global variable    ${available_pages}
    #Run keyword if    '${usr}' == 'BELUX'    Get available pages Belux
    #Run keyword if    '${usr}' == 'NL'    Get available pages Nl
    #Run keyword if    '${usr}' == 'GB'    Get available pages GB
    @{words}    String.Split String    ${available_pages}    ${SPACE}
    Set global variable    ${available_pages}    @{words}[-1]

Get available pages Belux
    Run keyword if    '${env}' == 'int'    Get available pages INT Belux
    Run keyword if    '${env}' == 'ecpt'    Get available pages ECPT Belux

Get available pages INT Belux
    ${available_pages}    SeleniumLibrary.Get Text    id=__BVID__276__BV_toggle_
    Set global variable    ${available_pages}

Get available pages ECPT Belux
    ${available_pages}    SeleniumLibrary.Get Text    id=__BVID__279__BV_toggle_
    Set global variable    ${available_pages}

Get available pages GB
    Run keyword if    '${env}' == 'int'    Get available pages INT GB
    Run keyword if    '${env}' == 'ecpt'    Get available pages ECPT GB

Get available pages INT GB
    ${available_pages}    SeleniumLibrary.Get Text    id=__BVID__276__BV_toggle_
    Set global variable    ${available_pages}

Get available pages ECPT GB
    ${available_pages}    SeleniumLibrary.Get Text    id=__BVID__267__BV_toggle_
    Set global variable    ${available_pages}

Get available pages Nl
    ${available_pages}    SeleniumLibrary.Get Text    id=__BVID__264__BV_toggle_
    Set global variable    ${available_pages}

Verify number of pages reflects pagination settings
    ${calculated_number_of_pages}    BuiltIn.Evaluate    ${total_products_quantity} / ${products_on_page}
    ${is_integer}    BuiltIn.Evaluate    ${total_products_quantity} % ${products_on_page}    #    calculates modulo
    ${calculated_number_of_pages}    BuiltIn.Convert to integer    ${calculated_number_of_pages}
    Set global variable    ${calculated_number_of_pages}
    Run keyword if    '${is_integer}' != '0'    Calculate number of pages for modulo different than zero
    Should be equal as integers    ${calculated_number_of_pages}    ${available_pages}

Calculate number of pages for modulo different than zero
    ${calculated_number_of_pages}    BuiltIn.Evaluate    ${calculated_number_of_pages} + 1
    Set global variable    ${calculated_number_of_pages}

Insert quantity value PLP
    Press Key    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/input    \\8
    SeleniumLibrary.Input Text    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/input    ${quantity_value}

Verify input field highlighted with error PLP
    SeleniumLibrary.Element should be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/input[contains(@class, 'col-6 ecp-quantity--input-error')]

Verify input field not highlighted with error PLP
    SeleniumLibrary.Element should not be visible    xpath=//div[contains(@class, 'products-overview')]/div[3]/div/div[${random int}]/div/div[3]/div[2]/div/div[2]/input[contains(@class, 'col-6 ecp-quantity--input-error')]

Show 36 products per page
    Unfold pagination option
    #Run keyword if    '${usr}' == 'NL'    Unfold pagination options Nl
    #Run keyword if    '${usr}' == 'BELUX'    Unfold pagination options Belux
    #Run keyword if    '${usr}' == 'GB'    Unfold pagination options GB
    Selenium Library.Click Element    css=.show > li:nth-child(2) > .dropdown-item

Show 75 products per page
    Unfold pagination option
    Choose 75 per page

Show 72 products per page
    Unfold pagination option
    Choose 72 per page

Choose 75 per page
    Selenium Library.Click Element    css=.show > li:nth-child(4) > .dropdown-item

Choose 72 per page
    Selenium Library.Click Element    css=.show > li:nth-child(3) > .dropdown-item

Unfold pagination option
    Selenium Library.Click Element    xpath=//div[2]/div/div[4]/div/div[3]/div/button    #xpath=//div[@class='row pagination']/div[3]/div/button

Unfold pagination options Nl
    Selenium Library.Click Element    id=__BVID__267__BV_toggle_

Unfold pagination options Belux
    Selenium Library.Click Element    id=__BVID__282__BV_toggle_

Unfold pagination options GB
    Selenium Library.Click Element    id=__BVID__270__BV_toggle_

Brand filter unfold by default BElUX
    Selenium Library.Element attribute value should be    id=collapse-Brand    class    collapse show

Flavour filter collapsed by default
    Selenium Library.Element should not be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div.card.collapse-multi-select.collapse-multi-select--open
    Selenium Library.Element should be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div.card.collapse-multi-select

Size filter collapsed by default BELUX
    Selenium Library.Element should not be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div.card.collapse-multi-select.collapse-multi-select--open
    Selenium Library.Element should be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div.card.collapse-multi-select

Packaging filter collapsed by default BELUX
    Selenium Library.Element should not be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(4) > div.card.collapse-multi-select.collapse-multi-select--open
    Selenium Library.Element should be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(4) > div.card.collapse-multi-select

Product Label filter collapsed by default BELUX
    Selenium Library.Element should not be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(5) > div.card.collapse-multi-select.collapse-multi-select--open
    Selenium Library.Element should be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(5) > div.card.collapse-multi-select

Verify product filter positioning BELUX
    ${brand_filter}    Selenium Library.Get Text    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(2) > div > div > div.collapse-multi-select__header
    BuiltIn.Should be equal as strings    ${brand_filter}    Brand
    #${flavour_filter}    Selenium Library.Get Text    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div > div > div.collapse-multi-select__header
    #BuiltIn.Should be equal as strings    ${flavour_filter}    Flavour
    ${size_filter}    Selenium Library.Get Text    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(3) > div > div > div.collapse-multi-select__header
    BuiltIn.Should be equal as strings    ${size_filter}    Size
    ${packaging_filter}    Selenium Library.Get Text    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(4) > div > div > div.collapse-multi-select__header
    BuiltIn.Should be equal as strings    ${packaging_filter}    Packaging
    ${product_label_filter}    Selenium Library.Get Text    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div:nth-child(5) > div > div > div.collapse-multi-select__header
    BuiltIn.Should be equal as strings    ${product_label_filter}    Product Label

Size filter collapsed by default GB
   Selenium Library.Element should not be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div > div.card.collapse-multi-select.collapse-multi-select--open
   Selenium Library.Element should be visible    css=#tab-products > div > div.shop__filter.col-lg-3.col-12 > div > div.form.d-none.d-lg-block > div > div.card.collapse-multi-select

Verify OR condition in brand filter BELUX
    Count brand elements
    Choose random brand
    Verify chip present in products overview

Count brand elements
    ${brand_elements}    Selenium Library.Get Element Count    css=div:nth-child(2) > .card #collapse-Brand .ecp-checkbox    #xpath=//div[@id='collapse-Brand']/div/div
    Set global variable    ${brand_elements}

Count size elements
    #${size_elements}    Selenium Library.Get Element Count    css=#collapse-Size > div > div.ecp-checkbox.custom-control.custom-checkbox
    #${size_elements}    Selenium Library.Get Element Count    xpath=//div[@id='collapse-Size']/div/div[@class='ecp-checkbox custom-control custom-checkbox']
    ${size_elements}    Selenium Library.Get Element Count    xpath=//label[contains(@for, 'Size')]
    ${size_elements}    BuiltIn.Evaluate    ${size_elements}/2
    Set global variable    ${size_elements}

Count packaging elements
    ${packaging_elements}    Selenium Library.Get Element Count    xpath=//label[contains(@for, 'Packaging')]
    ${packaging_elements}    BuiltIn.Evaluate    ${packaging_elements}/2
    Set global variable    ${packaging_elements}

Choose random brand
    ${random_int}    Evaluate    random.randint(1, ${brand_elements})    modules=random
    Set global variable    ${random_int}
    Selenium Library.Click element     css=div:nth-child(2) > .card #collapse-Brand .ecp-checkbox:nth-child(${random_int}) > .custom-control-label
    Sleep    3s
    Remember chosen brand name
    Remember items quantity for chosen brand

Choose random size
    ${random_int}    Evaluate    random.randint(1, ${size_elements})    modules=random
    Set global variable    ${random_int}
    Selenium Library.Click element     css=.collapse-multi-select--open #collapse-Size .ecp-checkbox:nth-child(${random_int}) > .custom-control-label
    Sleep    3s
    Remember chosen size name
    Remember items quantity for chosen size

Choose random packaging
    ${random_int}    Evaluate    random.randint(1, ${packaging_elements})    modules=random
    Set global variable    ${random_int}
    Selenium Library.Click element     css=.collapse-multi-select--open #collapse-Packaging .ecp-checkbox:nth-child(${random_int}) > .custom-control-label
    Sleep    3s
    Remember chosen packaging name
    Remember items quantity for chosen packaging

Remember chosen brand name
    ${name_with_quantity}    Selenium Library.Get Text    css=div:nth-child(2) > .card #collapse-Brand .ecp-checkbox:nth-child(${random_int}) .label
    Set global variable    ${name_with_quantity}
    @{chosen_name}    String.Split String From Right    ${name_with_quantity}    (    1
    Set global variable    ${chosen_brand_name}    @{chosen_name}[0]
    ${chosen_brand_name}    String.Strip string    ${chosen_brand_name}
    Set global variable    ${chosen_brand_name}

Remember chosen size name
    ${name_with_quantity}    Selenium Library.Get Text    css=#collapse-Size > div > div:nth-child(${random_int}) > label > span
    Set global variable    ${name_with_quantity}
    @{chosen_name}    String.Split String From Right    ${name_with_quantity}    (    1
    Set global variable    ${chosen_size_name}    @{chosen_name}[0]
    ${chosen_size_name}    String.Strip string    ${chosen_size_name}
    Set global variable    ${chosen_size_name}

Remember chosen packaging name
      ${name_with_quantity}    Selenium Library.Get Text    css=#collapse-Packaging > div > div:nth-child(${random_int}) > label > span
      Set global variable    ${name_with_quantity}
      @{chosen_name}    String.Split String From Right    ${name_with_quantity}    (    1
      Set global variable    ${chosen_packaging_name}    @{chosen_name}[0]
      ${chosen_packaging_name}    String.Strip string    ${chosen_packaging_name}
      Set global variable    ${chosen_packaging_name}

Remember items quantity for chosen brand
    @{chosen_quantity}    String.Split String From Right    ${name_with_quantity}    (    1
    Set global variable    ${chosen_brand_quantity}    @{chosen_quantity}[1]
    ${chosen_brand_quantity}    String.Fetch from left    ${chosen_brand_quantity}    )
    ${chosen_brand_quantity}    String.Strip string    ${chosen_brand_quantity}
    Set global variable    ${chosen_brand_quantity}

Remember items quantity for chosen size
    @{chosen_quantity}    String.Split String From Right    ${name_with_quantity}    (    1
    Set global variable    ${chosen_size_quantity}    @{chosen_quantity}[1]
    ${chosen_size_quantity}    String.Fetch from left    ${chosen_size_quantity}    )
    ${chosen_size_quantity}    String.Strip string    ${chosen_size_quantity}
    Set global variable    ${chosen_size_quantity}

Remember items quantity for chosen packaging
    @{chosen_quantity}    String.Split String From Right    ${name_with_quantity}    (    1
    Set global variable    ${chosen_packaging_quantity}    @{chosen_quantity}[1]
    ${chosen_packaging_quantity}    String.Fetch from left    ${chosen_packaging_quantity}    )
    ${chosen_packaging_quantity}    String.Strip string    ${chosen_packaging_quantity}
    Set global variable    ${chosen_packaging_quantity}

Verify Brand chip present in products overview
    ${chosen_brand_name}    String.Replace string    ${chosen_brand_name}    ${SPACE}    ${EMPTY}
    ${chosen_brand_name}    String.Replace string    ${chosen_brand_name}    /    ${EMPTY}
    ${chosen_brand_name}    String.Replace string    ${chosen_brand_name}    (    ${EMPTY}
    ${chosen_brand_name}    String.Replace string    ${chosen_brand_name}    )    ${EMPTY}
    ${chosen_brand_name}    String.Convert to lowercase    ${chosen_brand_name}
    Set global variable    ${chosen_brand_name}
    Selenium Library.Element should be visible    xpath=//div[@data-testid='ccep-div-chip-${chosen_brand_name}']

Verify Size chip present in products overview
    ${chosen_size_name}    String.Replace string    ${chosen_size_name}    ${SPACE}    ${EMPTY}
    ${chosen_size_name}    String.Replace string    ${chosen_size_name}    .    ${EMPTY}
    ${chosen_size_name}    String.Convert to lowercase    ${chosen_size_name}
    Set global variable    ${chosen_size_name}
    Selenium Library.Element should be visible    xpath=//div[@data-testid='ccep-div-chip-${chosen_size_name}']

Verify Packaging chip present in products overview
    ${chosen_packaging_name}    String.Replace string    ${chosen_packaging_name}    ${SPACE}    ${EMPTY}
    ${chosen_packaging_name}    String.Replace string    ${chosen_packaging_name}    (    ${EMPTY}
    ${chosen_packaging_name}    String.Replace string    ${chosen_packaging_name}    )    ${EMPTY}
    ${chosen_packaging_name}    String.Convert to lowercase    ${chosen_packaging_name}
    Set global variable    ${chosen_packaging_name}
    Selenium Library.Element should be visible    xpath=//div[@data-testid='ccep-div-chip-${chosen_packaging_name}']

Verify Brand products filtered
    Get all PLP products count
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${chosen_brand_quantity}

Verify Size products filtered
    Get all PLP products count
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${chosen_size_quantity}

Verify OR condition applied for Brand filter
    Get all PLP products count
    ${brands_quantity}    BuiltIn.Evaluate    ${chosen_brand_quantity} + ${chosen_brand_quantity1}
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${brands_quantity}

Verify OR condition applied for Size filter
    Get all PLP products count
    ${sizes_quantity}    BuiltIn.Evaluate    ${chosen_size_quantity} + ${chosen_size_quantity1}
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${sizes_quantity}

Remove filter by removing chip
    Scroll Page To Location    0    0
    Selenium Library.Set focus to element    xpath=//div[@data-testid='ccep-div-chip-${chosen_brand_name}']
    Selenium Library.Click element    xpath=//div[@data-testid='ccep-div-chip-${chosen_brand_name}']
    Selenium Library.Wait until element is not visible    xpath=//div[@data-testid='ccep-div-chip-${chosen_brand_name}']

Remove all filters
    Scroll Page To Location    0    0
    Selenium Library.Set focus to element     css=.products-overview__filters > div > .ecp-chip.mb-2.mb-0-md.ecp-chip--reset
    Selenium Library.Click element    css=.products-overview__filters > div > .ecp-chip.mb-2.mb-0-md.ecp-chip--reset    #xpath=//div[@class='ecp-chip mb-2 mb-0-md ecp-chip--reset']
    Selenium Library.Wait until element is not visible    css=.products-overview__filters > div > .ecp-chip.mb-2.mb-0-md.ecp-chip--reset

Verify filter removed
    Selenium Library.Element should not be visible    xpath=//div[@data-testid='ccep-div-chip-${chosen_brand_name}']
    Get all PLP products count
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${chosen_brand_quantity1}

Unfold size filter
    Selenium Library.Click element    xpath=//div[@class='card-body']/div[contains(text(),'Size')]

Unfold packaging filter
    Selenium Library.Click element    xpath=//div[@class='card-body']/div[contains(text(),'Packaging')]

Verify AND condition applied
    Get all PLP products count
    @{quantities}    Create list
    Collections.Append to list    ${quantities}    ${chosen_size_quantity}
    Collections.Append to list    ${quantities}    ${chosen_packaging_quantity}
    Set global variable    @{quantities}
    ${min_quantity}    ListOperations.Get min list value     ${quantities}
    BuiltIn.Should be equal as numbers    ${plp_products_count}    ${min_quantity}

Unfold brand filter
    Selenium Library.Click element    xpath=//div[@class='card-body']/div[contains(text(),'Brand')]

Insert search term into text filter
    Selenium Library.Input text    id=ecp-input-shop/products-search    ${search_term}
    Sleep    2s

Apply text filter
    Selenium Library.Click element    xpath=//i[@class='fa input-field__icon fa-search']

Verify filter applied
    Get total number of products
    :FOR    ${i}    IN RANGE    1    ${total_products_quantity}+1
    \    Set global variable    ${random_int}    ${i}    # this assignment is needed for Remember product name step
    \    Remember product name
    \    BuiltIn.Should contain    ${product_name}    ${search term}

Verify chip present in products overview
    ${search_term}    String.Convert to lowercase    ${search_term}
    Selenium Library.Element should be visible    xpath=//div[contains(@data-testid, 'ccep-div-chip-${search_term}')]

Verify only 1 product shown in search results
    Get total number of products
    Should be equal as numbers    ${total_products_quantity}    1
    Set global variable    ${random_int}    1
    Remember product name
    Remember product ean
    BuiltIn.Should be equal as strings    ${single_product_name}    ${product_name}
    BuiltIn.Should be equal as strings    ${single_product_ean}    ${product_ean}

Verify empty search results
    Get total number of products
    Should be equal as numbers    ${total_products_quantity}    0
